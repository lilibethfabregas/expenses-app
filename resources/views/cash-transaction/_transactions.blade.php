<tr>
    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
        {{ $bankTransaction->date->format('M j, Y') }}
    </td>

    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
        @if($bankTransaction->is_recorded)
            {{--            {{ dd($bankTransaction->journal->first()) }}--}}
            <a href="{{ route('journals.show', $bankTransaction->journal->first()) }}">
                {{ $bankTransaction->description }}
            </a>
        @else
            <a href="{{ route('bank-transactions.journals.create', $bankTransaction) }}">
                {{ $bankTransaction->description }}
            </a>
        @endif

    </td>
    {{--    <a href="journals/create/{{ $bankTransaction }}">--}}
    {{--        {{ $bankTransaction->description }}--}}
    {{--    </a>--}}

    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
        {{ number_format($bankTransaction->debit, 2) }}
    </td>

    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
        {{ number_format($bankTransaction->credit, 2) }}
    </td>

    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
        {{ number_format($bankTransaction->balance, 2) }}
    </td>
    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
        <toggle-update
            :checked="{{ $bankTransaction->is_recorded ? 'true' : 'false' }}"
            :route="'banks'"
            :model="{{ $bank }}"
            :toggleable="false"
        >
        </toggle-update>
    </td>
{{--    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">--}}
{{--        @if($bankTransaction->is_recorded && $bankTransaction->is_reconciled)--}}
{{--            Reconciled <a href="">View</a>--}}
{{--        @elseif($bankTransaction->is_recorded && ! $bankTransaction->is_reconciled)--}}
{{--            <a href="" class="">Reconcile</a>--}}
{{--        @endif--}}
{{--    </td>--}}

</tr>
