<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
               Create New Expense
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('banks.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left focus:outline-none"></i> Back to All Banks</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <form action="{{ route('banks.store') }}" method="POST" autocomplete="off">
            @csrf
            <input type="hidden" autocomplete="false">
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <div class="flex w-full justify-end items-center p-4">
                    <div class="flex">
                        <p class="shadow-sm mx-2">
                            <button class="focus:outline-none">
                                <i class="far fa-save py-2 px-3 shadow-sm bg-red-700 text-white rounded-sm"></i>
                            </button>
                        </p>
                    </div>
                </div>

                <div class="border-t border-gray-200">
                    <dl>
                        <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                            <dt class="text-xs font-medium text-gray-500">
                                Name
                            </dt>
                            <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                                <input
                                    name="particulars"
                                    placeholder="Lunch"
                                    class="w-11/12 p-2 focus:outline-none shadow-md rounded-md"
                                    value="{{ old('name') }}"
                                />
                                @if ($errors->has('name'))
                                    <small class="text-red-400 mx-2">{{ $errors->first('name') }}</small>
                                @endif
                            </dd>
                        </div>
                        <div class="bg-white p-4 grid grid-cols-3 gap-4">
                            <dt class="text-xs font-medium text-gray-500">
                                Account Number
                            </dt>
                            <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                                <input
                                    name="account_number"
                                    placeholder="200"
                                    class="w-11/12 p-2 focus:outline-none shadow-md rounded-md"
                                    value="{{ old('account_number') }}"
                                />
                                @if ($errors->has('account_number'))
                                    <small class="text-red-400 mx-2">{{ $errors->first('account_number') }}</small>
                                @endif
                            </dd>
                        </div>
                        <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                            <dt class="text-xs font-medium text-gray-500">
                                Account Name
                            </dt>
                            <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                                <input
                                    name="account_name"
                                    placeholder="Account Name"
                                    class="w-11/12 p-2 focus:outline-none shadow-md rounded-md"
                                    value="{{ old('account_name') }}"
                                />
                                @if ($errors->has('account_name'))
                                    <small class="text-red-400 mx-2">{{ $errors->first('account_name') }}</small>
                                @endif
                            </dd>
                        </div>
                    </dl>
                </div>
            </div>
        </form>
    </x-container>
</x-layout>
