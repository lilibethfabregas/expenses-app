<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                Cash
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('cash-transactions.index') }}" class="focus:outline-none pl-2"><i class="fas fa-arrow-left focus:outline-none"></i> Cash Transactions</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="p-4">
                <div class="flex">
                    Transaction Details
                </div>
            </div>

            <div class="border-t border-gray-200">
                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            placeholder=" "
                            label="Particulars"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ number_format($cashTransaction->journal->amount, 2) }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            placeholder=" "
                            label="Running Balance"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ number_format($cashTransaction->balance, 2) }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            placeholder=" "
                            label="Description"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ $cashTransaction->journal->description }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            placeholder=" "
                            label="Date"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ $cashTransaction->date->format('M j, Y h:s a') }}"
                        ></x-input>
                    </div>
                </div>
            </div>
        </div>
    </x-container>

{{--    <x-container>--}}
{{--        --}}{{--slot begins here --}}
{{--        <table class="min-w-full divide-y divide-gray-200">--}}
{{--            <thead class="bg-gray-50">--}}
{{--            <tr>--}}
{{--                <th scope="col" colspan="3"--}}
{{--                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">--}}
{{--                    Activity Logs--}}
{{--                </th>--}}
{{--            </tr>--}}
{{--            </thead>--}}
{{--            <tbody class="bg-white divide-y divide-gray-200">--}}
{{--            @forelse ($bank->activities as $activity)--}}
{{--                @include('partials._activities-list')--}}
{{--            @empty--}}
{{--                <tr>--}}
{{--                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">--}}
{{--                        <div class="flex items-center">--}}
{{--                            <div class="ml-4 text-xs">--}}
{{--                                No activities yet.--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            @endforelse--}}
{{--            </tbody>--}}
{{--        </table>--}}
{{--        --}}{{--slot ends here --}}
{{--    </x-container>--}}
</x-layout>
