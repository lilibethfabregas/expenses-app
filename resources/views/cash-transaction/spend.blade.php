<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
               Create New Journal Entry
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('cash-transactions.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left focus:outline-none"></i> Back to All Cash Transactions</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">

            <create-journal
                :accounts="{{ $accounts }}"
            ></create-journal>

        </div>
    </x-container>
</x-layout>
