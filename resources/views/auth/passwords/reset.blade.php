<x-layout>
    <div class="rounded">
        <h1 class="text-4xl text-center mb-12 font-thin text-gray-900 m-4">Reset Password</h1>
        <div class="w-full max-w-sm sm:w-96 relative bg-white overflow-hidden p-1.5 rounded-lg mx-auto shadow-lg">
            <form method="POST" action="{{ route('password.update') }}" class="m-4">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <x-input
                    name="email"
                    placeholder=" "
                    label="Email"
                    value="{{ old('email') }}"
                    error="{{ $errors->first('email') }}"
                ></x-input>

                <x-input
                    name="password"
                    placeholder=" "
                    label="Password"
                    type="password"
                    value="{{ old('password') }}"
                    error="{{ $errors->first('password') }}"
                ></x-input>

                <x-input
                    name="password_confirmation"
                    placeholder=" "
                    type="password"
                    label="Confirm Password"
                    value="{{ old('password_confirmation') }}"
                    error="{{ $errors->first('password_confirmation') }}"
                ></x-input>

                <button type="submit" class="w-full mt-4 bg-indigo-600 text-white rounded shadow custom-btn">
                    <span class="block px-8 py-2 text-gray-900"> {{ __('Reset Password') }}</span>
                </button>
            </form>

        </div>
    </div>
</x-layout>
