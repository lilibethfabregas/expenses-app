<x-layout>
    <div class="container mx-auto p-8 flex">
        <div class="max-w-md w-full mx-auto">
            <h1 class="text-4xl text-center mb-12 font-thin text-blue-900">Register</h1>

            <div class="bg-white rounded-lg overflow-hidden shadow-2xl">
                <div class="p-8">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="mb-5">
                            <label for="name" class="block mb-2 text-sm font-medium text-gray-600">Name</label>

                            <input id="name" type="text" class="block w-full p-3 rounded bg-gray-200 border border-transparent focus:outline-none
                                @error('email') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('email')
                            <span class="invalid-feedback text-red-700" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mb-5">
                            <label for="email" class="block mb-2 text-sm font-medium text-gray-600">Email</label>

                            <input id="email" type="email" class="block w-full p-3 rounded bg-gray-200 border border-transparent focus:outline-none
                                @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                            <span class="invalid-feedback text-red-700" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mb-5">
                            <label for="password" class="block mb-2 text-sm font-medium text-gray-600">Password</label>

                            <input id="password" type="password" class="block w-full p-3 rounded bg-gray-200 border border-transparent focus:outline-none
                            @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback text-red-700" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="mb-5">
                            <label for="password-confirm" class="block mb-2 text-sm font-medium text-gray-600">Confirm Password</label>

                            <input id="password-confirm" type="password" class="block w-full p-3 rounded bg-gray-200 border border-transparent focus:outline-none
                            @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">

                        </div>

                        <button type="submit" class="w-full p-3 mt-4 bg-indigo-600 text-white rounded shadow">Register</button>
                    </form>
                </div>

                <div class="flex justify-between p-8 text-sm border-t border-gray-300 bg-gray-100">
                    Already signed up? <a href="/login" class="font-medium text-indigo-500">Login</a>
                </div>
            </div>
        </div>
    </div>
</x-layout>
