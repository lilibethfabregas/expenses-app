<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body class="font-sans bg-gray-50">
<div id="app" class="relative min-h-screen">
    <div class="relative">
        <nav class="absolute top-0 w-full flex items-center justify-between flex-wrap pt-6 md:p-6">
            <div class="flex items-center flex-shrink-0 text-white mr-6">
                <h1 class="text-3xl font-extrabold">SPENDEE</h1>
            </div>

            <div class="block lg:hidden">
                <button class="flex items-center mr-6 px-6 py-2 border rounded text-teal-100 border-teal-400 hover:text-white hover:border-white" id="navbar-btn" onclick="toggleMenu()">
                    <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <title>Menu</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
                    </svg>
                </button>
            </div>

            <div class="w-full hidden md:block flex-grow lg:flex lg:items-center lg:w-auto transform duration-500" id="navbar">
                <div class="text-sm sm:flex-grow md:space-x-6 md:flex md:justify-end text-gray-800 md:text-white bg-gray-200 md:bg-transparent font-semibold">
                        <li class="no-underline block mt-4 sm:inline-block sm:mt-0 border-2 border-white rounded-3xl">
                            <a class="block px-8 py-3 focus:outline-none"
                               href="{{ url('authenticate') }}">Login / Register</a>
                        </li>
                </div>
            </div>
        </nav>
    </div>

    <div class="bg-gradient-to-b from-white to-white overflow-hidden">
        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path
                d="M0,0V46.29c47.79,22.2,103.59,32.17,158,28,70.36-5.37,136.33-33.31,206.8-37.5C438.64,32.43,512.34,53.67,583,72.05c69.27,18,138.3,24.88,209.4,13.08,36.15-6,69.85-17.84,104.45-29.34C989.49,25,1113-14.29,1200,52.47V0Z"
                opacity=".25" class="shape-fill" fill="#21AFD6" fill-opacity="1"/>
            <path
                d="M0,0V15.81C13,36.92,27.64,56.86,47.69,72.05,99.41,111.27,165,111,224.58,91.58c31.15-10.15,60.09-26.07,89.67-39.8,40.92-19,84.73-46,130.83-49.67,36.26-2.85,70.9,9.42,98.6,31.56,31.77,25.39,62.32,62,103.63,73,40.44,10.79,81.35-6.69,119.13-24.28s75.16-39,116.92-43.05c59.73-5.85,113.28,22.88,168.9,38.84,30.2,8.66,59,6.17,87.09-7.5,22.43-10.89,48-26.93,60.65-49.24V0Z"
                opacity=".5" class="shape-fill" fill="#21AFD6" fill-opacity="1"/>
            <path
                d="M0,0V5.63C149.93,59,314.09,71.32,475.83,42.57c43-7.64,84.23-20.12,127.61-26.46,59-8.63,112.48,12.24,165.56,35.4C827.93,77.22,886,95.24,951.2,90c86.53-7,172.46-45.71,248.8-84.81V0Z"
                class="shape-fill" fill="#21AFD6" fill-opacity="1"/>
        </svg>
        <div class="">
            <div class="h-screen relative">
                <div class="flex items-center h-screen md:h-1/2 text-gray-700">
                    <div class="w-8/12 mx-auto">
                        <div class="text-center">
                            <h1 class="text-2xl md:text-5xl uppercase">Control your finances.</h1>
                            <h1 class="md:text-3xl uppercase tracking-widest">Record your money in one place</h1>
                            <p class="mt-12 cursor-pointer"><a href="#features"
                                                               class="border-2 border-primary px-8 py-4 rounded-full focus:outline-none">Learn
                                    more</a></p>
                        </div>
                    </div>
                </div>

                <div class="absolute bottom-44 left-4 hidden md:block overflow-hidden" id="features">
                    <svg width="150" viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#C7EBF5"
                              d="M33.2,-40.4C46.1,-36.2,61.9,-30.8,70.2,-19.5C78.4,-8.2,79,8.8,74.1,24.1C69.2,39.4,58.7,52.8,45.4,64.3C32.1,75.9,16.1,85.4,-0.5,86.2C-17.1,86.9,-34.3,78.8,-45.2,66.6C-56.1,54.3,-60.8,37.8,-66.6,21.3C-72.4,4.9,-79.2,-11.5,-77.9,-28.4C-76.7,-45.3,-67.5,-62.7,-53.1,-66.4C-38.8,-70.2,-19.4,-60.2,-4.6,-53.8C10.1,-47.4,20.2,-44.6,33.2,-40.4Z"
                              transform="translate(100 100)"/>
                    </svg>

                </div>
                <div class="absolute bottom-20 left-12 hidden md:block overflow-hidden">
                    <svg width="240" viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#73cde5"
                              d="M36.2,-56.7C44.4,-51.1,46.9,-36.8,54.7,-23.8C62.5,-10.8,75.6,1,74.6,11C73.7,21.1,58.5,29.3,46.7,37.4C34.9,45.4,26.4,53.2,14.9,61.6C3.3,70,-11.3,79,-25.4,78.5C-39.5,78,-53.1,67.9,-61,55C-69,42,-71.4,26.3,-73.7,10.5C-76.1,-5.2,-78.4,-21.1,-71.4,-31.1C-64.5,-41.1,-48.2,-45.4,-34.7,-48.5C-21.2,-51.7,-10.6,-53.8,1.7,-56.4C14,-59.1,28,-62.2,36.2,-56.7Z"
                              transform="translate(100 100)"/>
                    </svg>
                </div>

                <div class="absolute top-32 -right-12 hidden md:block overflow-hidden">
                    <svg width="300" viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#c7ebf5"
                              d="M48.2,-63.7C60.9,-57.1,68.4,-40.9,74.9,-23.7C81.4,-6.5,86.9,11.8,84,29.3C81.1,46.8,69.8,63.7,54.4,70.6C39,77.5,19.5,74.4,2.5,71C-14.5,67.6,-29.1,63.8,-41.6,56C-54.1,48.1,-64.5,36.2,-68.5,22.6C-72.6,8.9,-70.2,-6.6,-64.2,-19.5C-58.1,-32.4,-48.4,-42.8,-37,-49.7C-25.7,-56.7,-12.9,-60.4,2.5,-63.8C17.8,-67.1,35.5,-70.2,48.2,-63.7Z"
                              transform="translate(100 100)"/>
                    </svg>
                </div>
            </div>

            <div class="mx-16 mb-32">
                <h1 class="text-3xl md:text-5xl font-extrabold text-center">FEATURES</h1>
                <div class="md:flex md:justify-center space-y-4 md:space-y-0 md:space-x-8 mt-8">
                    <div class="p-8 md:w-64 h-48 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <p class="text-center py-4"><i class="fas fa-upload text-center fa-3x text-primary"></i></p>
                        <h2>Import Bank Statements</h2>
                        <p></p>
                    </div>
                    <div class="p-8 md:w-64 h-48 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <p class="text-center py-4"><i
                                class="fas fa-balance-scale-right text-center fa-3x text-primary"></i></p>
                        <h2>Bank Reconciliation</h2>
                        <p></p>
                    </div>

                    <div class="p-8 md:w-64 h-48 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <p class="text-center py-4"><i class="fas fa-chart-pie text-center fa-3x text-primary"></i></p>
                        <h2>Expense Management</h2>
                        <p></p>
                    </div>

                    <div class="p-8 md:w-64 h-48 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <p class="text-center py-4"><i class="fas fa-chart-line text-center fa-3x text-primary"></i></p>
                        <h2>Stock Tracking</h2>
                        <p></p>
                    </div>

                </div>
            </div>

            <div class="mt-8 p-8 w-full bg-gray-50">
                <div class="mx-auto w-full md:w-6/12 p-8 text-center">
                    <h1 class="text-3xl md:text-5xl font-extrabold text-center uppercase">About Us</h1>
                    <p class="pt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquid
                        consectetur deleniti dolorum excepturi ipsa ipsum minima neque nulla quae quisquam reiciendis
                        reprehenderit sequi tempora, tempore velit voluptatem. Ab alias, aliquid aspernatur assumenda
                        consequatur delectus deleniti deserunt eius enim fugit illo minima natus necessitatibus neque
                        odio officia perferendis possimus provident quam reiciendis repudiandae sapiente similique sint
                        tenetur ut velit voluptates! Autem iure molestiae nesciunt perspiciatis saepe! A ad alias
                        aliquam animi, dolorem et hic inventore magnam minima, officiis omnis rem!</p>
                </div>
            </div>

            <div class="mx-4 md:mx-16 mt-8 my-32">
                <h1 class="text-3xl md:text-5xl font-extrabold text-center">TESTIMONIALS</h1>
                <div class="md:flex md:justify-center space-y-4 md:space-y-0 md:space-x-4 lg:space-x-8 mt-8 w-full">

                    <div class="py-8 px-2 lg:p-8 w-full md:w-6/12 xl:w-3/12 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <div class="w-32 h-32 mx-auto rounded-full"
                             style="background: url('https://cdn.psychologytoday.com/sites/default/files/styles/article-inline-half-caption/public/field_blog_entry_images/2018-09/shutterstock_648907024.jpg?itok=0hb44OrI') center center no-repeat; background-size: cover;"></div>
                        <div class="mt-4">
                            <h1 class="text-lg font-extrabold text-center">Karen Jackson</h1>
                            <p class="text-xs tracking-widest">Member since 2020</p>

                            <p class="mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet
                                aut nemo dolores eos totam ut.</p>
                        </div>
                    </div>

                    <div class="py-8 px-2 lg:p-8 w-full md:w-6/12 xl:w-3/12 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <div class="w-32 h-32 mx-auto rounded-full"
                             style="background: url('https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlcnxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80') center center no-repeat; background-size: cover;"></div>
                        <div class="mt-4">
                            <h1 class="text-lg font-extrabold text-center">Sam Kelly</h1>
                            <p class="text-xs tracking-widest">Member since 2020</p>

                            <p class="mt-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium amet
                                aut nemo
                                quidem sunt! Atque deleniti dolores eos totam ut.</p>
                        </div>
                    </div>

                    <div class="py-8 px-2 lg:p-8 w-full md:w-6/12 xl:w-3/12 rounded-3xl bg-background text-center"
                         style="box-shadow: 38px 38px 64px #d1d7e2, -38px -38px 64px #f1f7ff;">
                        <div class="w-32 h-32 mx-auto rounded-full"
                             style="background: url('https://img.freepik.com/free-photo/portrait-cheerful-attractive-young-woman-longsleeve-standing-with-arms-crossed-smiling_295783-39.jpg?size=626&ext=jpg&ga=GA1.2.1719178491.1616889600') center center no-repeat; background-size: cover;"></div>
                        <div class="mt-4">
                            <h1 class="text-lg font-extrabold text-center">Pia Simon</h1>
                            <p class="text-xs tracking-widest">Member since 2020</p>

                            <p class="mt-4">Amazing app!</p>
                            <p class="mt-4">All the functions I need are all here.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-8 px-4 md:px-8 pb-16 w-full bg-gray-50">
                <div class="mx-auto w-full md:w-6/12 p-8 text-center">
                    <h1 class="text-2xl font-extrabold text-center uppercase">Message from the Developer</h1>
                    <p class="pt-4">Being a CPA, I am used to tracking my own expenses, my income, and all my money put
                        in different places. I have used a lot of apps for that, but nothing really caters to my
                        needs. Hence, the making of this app. This is for my personal use, and it is architectured as to
                        how I need it.</p>

                    <p class="pt-4">This allow the user to import bank statement, and reconcile them. This allows the
                        record of your banks, your brokers, and can also track the user's stock transaction, as well as
                        the gains and loss.</p>

                    <p class="pt-4">The user can also record his/her expenses, be it personal expenses or expenses for
                        reimbursement, then record once the expense has been reimbursed. </p>

                    <p class="pt-4">The user can customise chart of accounts.</p>
                </div>
            </div>

        </div>
    </div>
    <div class="bg-gray-50">
        <svg data-name="Layer 1"
             xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path
                d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z"
                class="shape-fill"
                fill="#21AFD6" fill-opacity="1"/>
        </svg>
    </div>
</div>
</body>

<script>
    function toggleMenu(event) {
        let navbar = document.getElementById('navbar')
        navbar.classList.toggle("hidden");
    }
</script>
</html>
