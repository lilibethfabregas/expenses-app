<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                Updating {{ $ability->name }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('abilities.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left"></i> Back to All Abilities</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <toggle-button
                    :deleted="{{ $ability->trashed() ? 'true' : 'false' }}"
                    :route="'abilities'"
                    :model="{{ $ability }}"
                ></toggle-button>
            </div>

            <div class="border-t border-gray-200">
                <dl>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Name
                        </dt>
                        <form action="{{ route('abilities.update', $ability) }}" method="post" class="col-span-2">
                            @csrf @method('PUT')
                            <dd class="text-xs text-gray-900 sm:mt-0 flex">
                                <input
                                    name="name"
                                    placeholder=" "
                                    class="w-11/12 p-2 focus:outline-none"
                                    value="{{ old('name') ? old('name') : $ability->name }}"
                                />

                                <button class="md:mx-2 focus:outline-none bg-primary rounded-xl shadow-lg px-4 py-2 text-white">
                                    Save
                                </button>
                            </dd>
                        </form>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Slug
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $ability->slug }}
                        </dd>
                    </div>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Created
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $ability->created_at->diffForHumans() }}
                        </dd>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Updated
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $ability->updated_at->diffForHumans() }}
                        </dd>
                    </div>
                </dl>
            </div>
        </div>
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($ability->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
