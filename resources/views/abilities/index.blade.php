<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('abilities.index') }}" class="focus:outline-none pl-2">Abilities</a>
            </h5>
            <x-search-form action="/abilities/search" method="POST" role="search"/>
        </div>
        <create :model="'ability'" :route="'/abilities'"></create>
    </x-page-header>


    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Name
                </th>
                <th scope="col" class="px-2 md:px-6 py-3 hidden lg:block">
                    <Slug></Slug>
                </th>
                <th scope="col" class="px-2 md:px-6 py-3 text-right">
                    Active
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($abilities as $ability)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ $ability->trashed() ? '' : route('abilities.show', $ability) }}">
                                        {{ $ability->name }}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Created {{ $ability->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500 hidden lg:block">
                        {{ $ability->slug }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-sm text-gray-500 text-right">
                        <toggle-button
                            :deleted="{{ $ability->trashed() ? 'true' : 'false' }}"
                            :route="'abilities'"
                            :model="{{ $ability }}"
                        ></toggle-button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            @if($abilities->hasPages())
                <x-paginator
                    count="{{ $count }}"
                    model="abilities"
                    hasNextPage="{{ $abilities->hasMorePages() }}"
                    currentPage="{{ $abilities->currentPage() }}"
                    nextPageUrl="{{ $abilities->nextPageUrl() }}"
                    prevPageUrl="{{ $abilities->previousPageUrl() }}"
                >
                </x-paginator>
            @endif

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse

            @if($activities->hasPages())
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        {{ $activities->links() }}
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
