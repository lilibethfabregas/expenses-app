<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                {{ $user->name }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('users.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left"></i> Back to All Users</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <div class="p-4 flex">
                    <p class="shadow-sm cursor-pointer" title="block user"><i class="fas fa-ban py-2 px-4 shadow-sm bg-gray-100 rounded-sm"></i></p>
                </div>
            </div>

            <div class="border-t border-gray-200">
                <dl>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Name
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $user->name }}
                        </dd>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Role
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $user->role->name }}
                        </dd>
                    </div>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Slug
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $user->slug }}
                        </dd>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">

                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">

                        </dd>
                    </div>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Joined
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $user->created_at->diffForHumans() }}
                        </dd>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Updated
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $user->updated_at->diffForHumans() }}
                        </dd>
                    </div>
                </dl>
            </div>
        </div>
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($user->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
