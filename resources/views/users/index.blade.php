<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('users.index') }}" class="focus:outline-none pl-2">Users</a>
            </h5>
            <x-search-form action="/users/search" method="POST" role="search"/>
        </div>
    </x-page-header>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Name
                </th>
                <th scope="col" class="px-2 md:px-6 py-3 hidden lg:block">
                    Role
                </th>
                <th scope="col" class="px-2 md:px-6 py-3 text-right">
                    Active
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($users as $user)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ $user->trashed() ? '' : route('users.show', $user) }}">
                                        {{ $user->name }}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Joined {{ $user->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500 hidden lg:block">
                        <a class="focus:outline-none"
                           href="{{ $user->role->trashed() ? '' : route('roles.show', $user->role) }}">{{ $user->role->name }}</a>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-sm text-gray-500 text-right">
                        <toggle-button
                            :deleted="{{ $user->trashed() ? 'true' : 'false' }}"
                            :route="'users'"
                            :model="{{ $user }}"
                        ></toggle-button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            @if($users->hasPages())
                <x-paginator
                    count="{{ $count }}"
                    model="users"
                    hasNextPage="{{ $users->hasMorePages() }}"
                    currentPage="{{ $users->currentPage() }}"
                    nextPageUrl="{{ $users->nextPageUrl() }}"
                    prevPageUrl="{{ $users->previousPageUrl() }}"
                >
                </x-paginator>
            @endif

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse

            @if($activities->hasPages())
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        {{ $activities->links() }}
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
