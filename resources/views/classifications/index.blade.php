<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('classifications.index') }}" class="focus:outline-none pl-2">Classifications</a>
            </h5>
            <x-search-form action="/classifications/search" method="POST" role="search"/>
        </div>
        <create :model="'category'" :route="'/classifications'"></create>
    </x-page-header>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Name
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Slug
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Active
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($classifications as $classification)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ $classification->trashed() ? '' : route('classifications.show', $classification) }}">
                                        {{ ucfirst($classification->name) }}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Created {{ $classification->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ $classification->slug }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        <toggle-button
                            :deleted="{{ $classification->trashed() ? 'true' : 'false' }}"
                            :route="'classifications'"
                            :model="{{ $classification }}"
                        ></toggle-button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="4">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            @if($classifications->hasPages())
                <x-paginator
                    count="{{ $classifications->count() }}"
                    model="classifications"
                    hasNextPage="{{ $classifications->hasMorePages() }}"
                    currentPage="{{ $classifications->currentPage() }}"
                    nextPageUrl="{{ $classifications->nextPageUrl() }}"
                    prevPageUrl="{{ $classifications->previousPageUrl() }}"
                >
                </x-paginator>
            @endif

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse
            @if($activities->hasPages())
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        {{ $activities->links() }}
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
