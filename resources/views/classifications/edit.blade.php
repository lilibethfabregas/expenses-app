<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                Updating {{ $classification->name }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('classifications.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left"></i> Back to All Classifications</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <toggle-button
                    :deleted="{{ $classification->trashed() ? 'true' : 'false' }}"
                    :route="'classifications'"
                    :model="{{ $classification }}"
                ></toggle-button>
            </div>

            <div class="border-t border-gray-200">
                <dl>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Name
                        </dt>
                        <form action="{{ route('classifications.update', $classification) }}" method="post" class="col-span-2">
                            @csrf @method('PUT')
                            <dd class="text-xs text-gray-900 sm:mt-0 flex">
                                <input
                                    name="name"
                                    placeholder=" "
                                    class="w-11/12 p-2 focus:outline-none"
                                    value="{{ old('name') ? old('name') : $classification->name }}"
                                />

                                <button class="md:mx-2 focus:outline-none">
                                    <i class="far fa-save py-2 px-3 shadow-sm bg-red-700 text-white rounded-sm"></i>
                                </button>
                            </dd>
                        </form>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Slug
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $classification->slug }}
                        </dd>
                    </div>
                    <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Created
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $classification->created_at->diffForHumans() }}
                        </dd>
                    </div>
                    <div class="bg-white p-4 grid grid-cols-3 gap-4">
                        <dt class="text-xs font-medium text-gray-500">
                            Updated
                        </dt>
                        <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                            {{ $classification->updated_at->diffForHumans() }}
                        </dd>
                    </div>
                </dl>
            </div>
        </div>
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($classification->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
