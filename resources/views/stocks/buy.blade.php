<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('stocks.index') }}" class="focus:outline-none pl-2">Stocks</a>
            </h5>
{{--            <x-search-form action="/stocks/search" method="POST" role="search"/>--}}

        </div>
    </x-page-header>
    <div class="text-primary text-center">
        @if (session()->has('flash'))
            <i class="fas fa-check-double"></i> {{ session()->get('flash') }}
        @endif
    </div>
    <x-container>
        {{--slot begins here --}}
        <form action="{{ route('stock-transaction.store') }}" method="POST">
            @csrf
            <input type="hidden" name="type" value="BN">
            <div class="border-t border-gray-200">
                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="stock"
                            placeholder=" "
                            label="Stock name"
                            width="w-11/12 p-2"
                            value="{{ old('stock') }}"
                            error="{{ $errors->first('stock') }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="shares"
                            placeholder=" "
                            label="Number of shares"
                            width="w-11/12 p-2"
                            value="{{ old('shares') }}"
                            error="{{ $errors->first('shares') }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="price"
                            placeholder=" "
                            label="Price"
                            width="w-11/12 p-2"
                            value="{{ old('price') }}"
                            error="{{ $errors->first('price') }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <options-without-autosave
                            :choices="{{ auth('web')->user()->brokers }}"
                            :label="'Broker'"
                            :name="'broker_id'"
                            :passed-value="'{{ old('broker_id') }}'"
                        ></options-without-autosave>

                        @if ($errors->has('broker_id'))
                            <small class="text-red-400 mx-2">{{ $errors->first('broker_id') }}</small>
                        @endif
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <button type="submit"
                                class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                            <div class="py-1 px-4 text-white font-bold">
                                Save
                            </div>
                            <div class="bg-white px-3 flex flex-shrink-0 py-3">
                                <i class="far fa-save fa-lg text-primary"></i>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </form>
        {{--slot ends here --}}
    </x-container>
</x-layout>
