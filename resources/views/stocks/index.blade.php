<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('stocks.index') }}" class="focus:outline-none pl-2">Stocks</a>
            </h5>
        </div>
        <div class="px-4 py-2 text-xs flex space-x-4 flex-wrap">

          <div class="w-24 my-2">
              <a href="{{ route('stock-transaction.buy.create') }}"
                 class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                  <div class="py-2 px-4 text-white font-bold">
                      Buy
                  </div>
                  <div class="bg-white px-3 flex flex-shrink-0 py-3">
                      <i class="fas fa-money-bill-wave fa-lg text-primary"></i>
                  </div>
              </a>
          </div>

           <div class="w-24 my-2">
               <a href="{{ route('stock-transaction.sell.create') }}"
                  class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                   <div class="py-2 px-4 text-white font-bold">
                       Sell
                   </div>
                   <div class="bg-white px-3 flex flex-shrink-0 py-3">
                       <i class="fas fa-comment-dollar fa-lg text-primary"></i>
                   </div>
               </a>
           </div>

            <add-new-broker></add-new-broker>

        </div>
    </x-page-header>

    <x-page-header>
        <div class="my-4 flex w-9/12 space-x-5">
            <div class="mt-5">
                <small>Shares Summary</small>
                <div class="grid grid-cols-12 bg-primaryLightest text-white">
                    <div class="col-span-6 text-center p-1 border-r">
                        Stock
                    </div>
                    <div class="col-span-6 text-center p-1">
                        Shares
                    </div>
                </div>
                <div class="grid grid-cols-12 text-gray-900">
                    @forelse($shares as $share)
                        @if ($share->total > 0)
                            <div class="col-span-6 text-center p-1 border-r border-b border-gray-400">
                                {{ $share->stock }}
                            </div>
                            <div class="col-span-6 text-center p-1 border-b border-gray-400">
                                {{  number_format($share->total) }}
                            </div>
                        @endif
                    @empty
                        <div class="col-span-12 text-gray-500 text-center p-1 border-r border-b border-gray-400">
                            <small>No stocks currently.</small>
                        </div>
                    @endforelse
                </div>
            </div>

            <div class="mt-5">
                <small>Buying Power</small>
                <div class="grid grid-cols-12 bg-primaryLightest text-white">
                    <div class="col-span-6 text-center border-r p-1">
                        Broker
                    </div>
                    <div class="col-span-6 text-center p-1">
                        BP
                    </div>
                </div>
                <div class="grid grid-cols-12 text-gray-900">
                    @forelse($buyingPower as $cash)
                        <div class="col-span-6 text-center p-1 border-r border-b border-gray-400">
                            {{ $cash['name'] }}
                        </div>
                        <div class="col-span-6 text-center p-1 border-b border-gray-400">
                            {{ number_format($cash['sum'], 2) }}
                        </div>
                    @empty
                        <div class="col-span-12 text-center p-1 border-b border-gray-400">
                            <small>No brokers currently</small>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>

    </x-page-header>
    <div class="text-primary text-center">
        @if (session()->has('flash'))
            <i class="fas fa-check-double"></i> {{ session()->get('flash') }}
        @endif
    </div>
    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Description
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Total
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Broker
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($stocks as $stock)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ route('stocks.show', $stock) }}">
                                        @switch($stock->type)
                                            @case('DE')
                                            Deposited.
                                            @break

                                            @case('BN')
                                            Bought {{ number_format($stock->shares) }} of {{ $stock->stock }}.
                                            @break

                                            @case('SN')
                                            Sold {{ number_format($stock->shares) * -1 }} of {{ $stock->stock }}.
                                            @break

                                            @case('WD')
                                            Withdrew.
                                            @break
                                        @endswitch

{{--                                        {{ ucfirst($stock->journal->description) }}--}}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Created {{ $stock->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ number_format($stock->total, 2) }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        <options-with-model-update
                            :choices="{{ $brokers }}"
                            :route="'/brokers'"
                            :label="'Broker'"
                            :name="'broker'"
                            :passed-value="'{{ $stock->broker }}'"
                            :model="{{ $stock }}"
                            :edit-whole-entry="true"
                            :edit-route="'/stock-transaction'"
                        ></options-with-model-update>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="4">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activities
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse
            {{--            @if($activities->hasPages())--}}
            {{--                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">--}}
            {{--                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">--}}
            {{--                        {{ $activities->links() }}--}}
            {{--                    </td>--}}
            {{--                </tr>--}}
            {{--            @endif--}}
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
