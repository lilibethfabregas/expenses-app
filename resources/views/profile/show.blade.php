<x-layout>
    <x-page-header>
        <div class="my-1">
            <p class="max-w-2xl text-xs text-gray-500 text-left">
{{--                <a href="{{ route('roles.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left"></i> Back to All Roles</a>--}}
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="border-t">
            <account-config></account-config>
        </div>
    </x-container>
</x-layout>


