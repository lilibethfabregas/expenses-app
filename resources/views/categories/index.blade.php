<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('categories.index') }}" class="focus:outline-none pl-2">Categories</a>
            </h5>
            <x-search-form action="/categories/search" method="POST" role="search"/>
        </div>
        <create :model="'category'" :route="'/categories'"></create>
    </x-page-header>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Name
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Slug
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Active
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($categories as $category)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ $category->trashed() ? '' : route('categories.show', $category) }}">
                                        {{ ucfirst($category->name) }}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Created {{ $category->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ $category->slug }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        <toggle-button
                            :deleted="{{ $category->trashed() ? 'true' : 'false' }}"
                            :route="'categories'"
                            :model="{{ $category }}"
                        ></toggle-button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="4">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            @if($categories->hasPages())
                <x-paginator
                    count="{{ $categories->count() }}"
                    model="categories"
                    hasNextPage="{{ $categories->hasMorePages() }}"
                    currentPage="{{ $categories->currentPage() }}"
                    nextPageUrl="{{ $categories->nextPageUrl() }}"
                    prevPageUrl="{{ $categories->previousPageUrl() }}"
                >
                </x-paginator>
            @endif

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse
            @if($activities->hasPages())
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        {{ $activities->links() }}
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
