<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('banks.index') }}" class="focus:outline-none pl-2">Banks</a>
            </h5>
            <x-search-form action="/banks/search" method="POST" role="search"/>
        </div>
        <div class="px-4 py-2 text-xs my-2">
          <div class="w-28">
              <a href="{{ route('banks.create') }}" class="flex bg-primary rounded-xl overflow-hidden border border-gray-400 shadow-lg focus:outline-none">
                  <div class="py-2 px-4 text-white font-bold">Create</div>
                  <div class="bg-white px-3 flex flex-shrink-0 py-3">
                      <i class="fas fa-plus fa-lg text-primary"></i>
                  </div>
              </a>
          </div>
        </div>
    </x-page-header>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Name
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Account Number
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Balance
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Active
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($banks as $bank)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ route('banks.show', $bank) }}">
                                        {{ ucfirst($bank->name) }}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Created {{ $bank->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ $bank->account_number }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ number_format($bank->balance(), 2) }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        <toggle-button
                            :deleted="{{ $bank->trashed() ? 'true' : 'false' }}"
                            :route="'banks'"
                            :model="{{ $bank }}"
                        ></toggle-button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="4">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse
            @if($activities->hasPages())
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        {{ $activities->links() }}
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{--slot ends here --}}

    </x-container>
</x-layout>
