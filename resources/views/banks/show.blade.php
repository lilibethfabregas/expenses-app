<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                {{ ucfirst($bank->name) }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('banks.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left"></i> Back to All Banks</a>
            </p>
        </div>
    </x-page-header>
    @if (session()->has('flash'))
        {{ session()->get('flash') }}
    @endif
    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <div class="flex space-x-4 relative">
                    <a href="{{ route('banks.edit', $bank) }}" class="flex flex-shrink-0 flex-grow-0 bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                        <div class="py-2 px-4 text-white font-bold">
                            Update
                        </div>
                        <div class="bg-white px-3 flex flex-shrink-0 py-3">
                            <i class="far fa-edit fa-lg text-primary"></i>
                        </div>
                    </a>

                    <form action="{{ route('banks.destroy', $bank) }}" method="POST" class="group">
                        @csrf @method('DELETE')
                        <button class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none" {{ $bank->transactions ? 'disabled' : '' }}>
                            <div class="py-2 px-4 text-white font-bold">
                                Delete
                            </div>
                            <div class="bg-white px-3 flex flex-shrink-0 py-3">
                                <i class="far fa-trash-alt fa-lg text-primary"></i>
                            </div>
                        </button>
                        <div class="opacity-0 mb-6 group-hover:opacity-100 duration-300 absolute top-11 bg-primaryLighter text-white p-2 rounded-md w-72 right-0">
                            <small>To delete, please make sure this has no transactions.</small>
                        </div>
                    </form>
                </div>
            </div>

            <div class="border-t border-gray-200">
                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="name"
                            placeholder=" "
                            label="Bank Name"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ $bank->name }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="particulars"
                            placeholder=" "
                            label="Particulars"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ $bank->account_number }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="account_name"
                            placeholder=" "
                            label="Account Name"
                            width="w-11/12 p-2"
                            readonly="readonly"
                            value="{{ $bank->account_name }}"
                        ></x-input>
                    </div>
                </div>
            </div>
        </div>
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <div class="flex">
                    <form action="{{ url('bank-transactions') }}" method="POST" enctype="multipart/form-data" class="flex space-x-4">
                        @csrf
                        <div class="relative w-36 text-center mx-auto">
                              <span class="relative z-0 inline-block w-full border border-primary text-gray-00 p-1 text-sm">
                                Select Bank Statement
                              </span>
                            <input type="file" name="file" class="inline-block absolute z-1 w-full top-0 left-0 opacity-0 cursor-pointer" placeholder="Upload File">

                            @if ($errors->has('file'))
                                <small class="text-red-400 mx-2">{{ $errors->first('file') }}</small>
                            @endif
                        </div>

                        <input type="hidden" value="{{ $bank->id }}" name="bank_id">

                        <button class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                            <div class="py-2 px-4 text-white font-bold">
                                Upload
                            </div>
                            <div class="bg-white px-3 flex flex-shrink-0 py-3">
                                <i class="fas fa-upload fa-lg text-primary"></i>
                            </div>
                        </button>
                    </form>
                </div>
            </div>
            <table class="min-w-full divide-y divide-gray-200 text-xs">
                <thead class="bg-gray-100">
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Date
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Description
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Debit
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Credit
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Balance
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Recorded?
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">

                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @forelse ($bank->transactions as $bankTransaction)
                    @include('banks._transactions')
                @empty
                    <tr>
                        <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="4">
                            No data yet.
                        </td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>

        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($bank->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
