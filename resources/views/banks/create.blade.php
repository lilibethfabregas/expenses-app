<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                Create New Bank Account
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('banks.index') }}" class="focus:outline-none"><i
                        class="fas fa-arrow-left focus:outline-none"></i> Back to All Banks</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <form action="{{ route('banks.store') }}" method="POST" autocomplete="off">
            @csrf
            <input type="hidden" autocomplete="false">
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <div class="flex w-full justify-end items-center p-4">
                    <div class="flex">
                        <button class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                            <div class="py-2 px-4 text-white font-bold">
                                Save
                            </div>
                            <div class="bg-white px-3 flex flex-shrink-0 py-3">
                                <i class="far fa-save fa-lg text-primary"></i>
                            </div>
                        </button>
                    </div>
                </div>

                <div class="border-t border-gray-200">
                    <dl>
                        <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                            <dt class="text-xs font-medium text-gray-500">

                            </dt>
                            <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                                <x-input
                                    name="name"
                                    placeholder=" "
                                    label="Bank name"
                                    width="w-11/12 p-2"
                                    value="{{ old('name') }}"
                                ></x-input>
                            </dd>
                        </div>
                        <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                            <dt class="text-xs font-medium text-gray-500">

                            </dt>
                            <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                                <x-input
                                    name="account_number"
                                    placeholder=" "
                                    label="Account Number"
                                    width="w-11/12 p-2"
                                    value="{{ old('account_number') }}"
                                ></x-input>
                            </dd>
                        </div>
                        <div class="bg-gray-50 p-4 grid grid-cols-3 gap-4">
                            <dt class="text-xs font-medium text-gray-500">

                            </dt>
                            <dd class="text-xs text-gray-900 sm:mt-0 col-span-2">
                                <x-input
                                    name="account_name"
                                    placeholder=" "
                                    label="Account Name"
                                    width="w-11/12 p-2"
                                    value="{{ old('account_name') }}"
                                ></x-input>
                            </dd>
                        </div>
                    </dl>
                </div>
            </div>
        </form>
    </x-container>
</x-layout>
