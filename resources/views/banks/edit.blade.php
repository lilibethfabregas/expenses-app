<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                {{ ucfirst($bank->name) }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('banks.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left focus:outline-none"></i> Back to All Banks</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <form action="{{ route('banks.update', $bank) }}" method="POST" autocomplete="off">
            @csrf @method('PUT')
            <input type="hidden" autocomplete="false">
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <div class="flex">
                    <button
                        class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                        <div class="py-1 px-4 text-white font-bold">
                            Save
                        </div>
                        <div class="bg-white px-3 flex flex-shrink-0 py-3">
                            <i class="far fa-save fa-lg text-primary"></i>
                        </div>
                    </button>
                </div>
            </div>
            <div class="border-t border-gray-200">
                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="name"
                            placeholder=" "
                            label="Bank Name"
                            width="w-11/12 p-2"
                            value="{{ old('name') ? old('name') : $bank->name }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="account_number"
                            placeholder=" "
                            label="Account Number"
                            width="w-11/12 p-2"
                            value="{{ old('account_number') ? old('account_number') : $bank->account_number }}"
                            error="{{ $errors->first('account_number') }}"
                        ></x-input>
                    </div>
                </div>

                <div class="p-4 grid grid-cols-3 gap-4">
                    <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                        <x-input
                            name="account_name"
                            placeholder=" "
                            label="Account Name"
                            width="w-11/12 p-2"
                            value="{{ old('account_name') ? old('account_name') : $bank->account_name }}"
                            error="{{ $errors->first('account_name') }}"
                        ></x-input>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($bank->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
