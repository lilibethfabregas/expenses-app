@php
    $model = str_replace('App\Models\\', '', $activity->subject_type);
    $tables = Str::plural(strtolower($model));
    $name = $activity->subject_type::withTrashed()->whereKey($activity->subject_id)->first()->name;
    $title = $activity->subject_type::withTrashed()->whereKey($activity->subject_id)->first()->title;
    $id = $activity->subject_type::withTrashed()->whereKey($activity->subject_id)->first()->id;
    $slug = $activity->subject_type::withTrashed()->whereKey($activity->subject_id)->first()->slug;
@endphp

<tr>
    <td class="px-1 py-4 whitespace-nowrap">
        <div class="flex items-center">
            <div class="ml-4">
                <div class="text-gray-900 text-xs">
                    <a class=""
                       href="{{ route('roles.show', $activity->user->role) }}">{{$activity->user->role->name }}</a>
                    <a class="" href="{{ route('users.show', $activity->user) }}">{{$activity->user->name }}</a>

                     {{ $activity->event }} the {{ strtolower($model) }}

                        @if ($activity->event == 'deleted')
                            @if(\Illuminate\Support\Facades\Schema::hasColumn($tables, 'name'))
                                {{ $name }}
                            @elseif(\Illuminate\Support\Facades\Schema::hasColumn($tables, 'title'))
                                {{ $title }}
                            @else
                                with ID: {{ $id }}
                            @endif
                        @else
                            @if((\Illuminate\Support\Facades\Schema::hasColumn($tables, 'slug')))
                                <a class=""
                                   href="/{{$tables}}/{{$slug}}">
                                    @if(\Illuminate\Support\Facades\Schema::hasColumn($tables, 'name'))
                                        {{ $name }}
                                    @elseif(\Illuminate\Support\Facades\Schema::hasColumn($tables, 'title'))
                                        {{ $title }}
                                    @else
                                        with ID: {{ $id }}
                                    @endif
                             </a>.
                            @else
                                <a class=""
                                   href="/{{$tables}}/{{$id}}">
                                    @if(\Illuminate\Support\Facades\Schema::hasColumn($tables, 'name'))
                                        {{ $name }}
                                    @elseif(\Illuminate\Support\Facades\Schema::hasColumn($tables, 'title'))
                                        {{ $title }}
                                    @else
                                        with ID: {{ $id }}
                                    @endif
                                 </a>.
                            @endif
                        @endif
                </div>

                <div class="text-gray-500 text-xs">
                    Created {{ $activity->created_at->diffForHumans() }}
                </div>
            </div>
        </div>
    </td>
</tr>

