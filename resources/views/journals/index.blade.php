<x-layout>
    <x-page-header>
        <div class="my-4">
            <h5 class="text-xl font-thin text-primary">
                <a href="{{ route('journals.index') }}" class="focus:outline-none pl-2">Journals</a>
            </h5>
            <x-search-form action="/journals/search" method="POST" role="search"/>
        </div>
        <div class="px-4 py-2 text-xs my-2">
            <p><a href="{{ route('journals.create') }}" class="rounded bg-red-900 text-xs font-bolder hover:bg-primary py-2 px-4 my-4 text-white cursor-pointer focus:outline-none">Create Bank</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200 text-xs">
            <thead class="bg-gray-100">
            <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                <th scope="col" class="px-2 md:px-6 py-3">
                    Name
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Desc
                </th>
                <th scope="col" class="px-2 md:px-6 py-3">
                    Active
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($journals as $journal)
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
                        <div class="flex items-center">
                            <div>
                                <div class="">
                                    <a class="focus:outline-none"
                                       href="{{ route('journals.show', $journal) }}">
                                        {{ ucfirst($journal->description) }}
                                    </a>
                                </div>
                                <div class="text-gray-500">
                                    <small> Created {{ $journal->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ $journal->debit_amount }}
                    </td>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
{{--                        <toggle-button--}}
{{--                            :deleted="{{ $journal->trashed() ? 'true' : 'false' }}"--}}
{{--                            :route="'banks'"--}}
{{--                            :model="{{ $journal }}"--}}
{{--                        ></toggle-button>--}}
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="4">
                        No data yet.
                    </td>
                </tr>
            @endforelse

            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-xs" colspan="3">
                        No activities yet.
                    </td>
                </tr>
            @endforelse
            @if($activities->hasPages())
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap" colspan="3">
                        {{ $activities->links() }}
                    </td>
                </tr>
            @endif
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
