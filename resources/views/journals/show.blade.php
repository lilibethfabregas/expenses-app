<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                {{ $journal->id }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('journals.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left"></i>
                    Back to All Journal Entries</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="flex w-full justify-end items-center p-4">
                <div class="flex space-x-4 relative">
                    <a href="{{ route('journals.edit', $journal) }}" class="flex flex-shrink-0 flex-grow-0 bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                        <div class="py-2 px-4 text-white font-bold">
                            Update
                        </div>
                        <div class="bg-white px-3 flex flex-shrink-0 py-3">
                            <i class="far fa-edit fa-lg text-primary"></i>
                        </div>
                    </a>

                    <form action="{{ route('journals.destroy', $journal) }}" method="POST">
                        @csrf @method('DELETE')
                        <button class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                            <div class="py-2 px-4 text-white font-bold">
                                Delete
                            </div>
                            <div class="bg-white px-3 flex flex-shrink-0 py-3">
                                <i class="far fa-trash-alt fa-lg text-primary"></i>
                            </div>
                        </button>
                    </form>
                </div>
            </div>

            <table class="min-w-full divide-y divide-gray-200 text-xs">
                <thead class="bg-gray-100">
                <tr class="font-bolder text-left tracking-wider uppercase text-gray-700">
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Account
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Description
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Debit
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Credit
                    </th>
                    <th scope="col" class="px-2 md:px-6 py-3">
                        Track?
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @forelse($transaction as $entry)
                    <tr>
                        <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                            {{ $entry->account }}
                        </td>
                        <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                            {{ $entry->description }}
                        </td>
                        <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                            {{ $entry->amount < 0 ? "" : $entry->amount}}
                        </td>

                        <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                            {{ $entry->amount > 0 ? "" : $entry->amount * -1}}
                        </td>

                        <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                            {{ $entry->isTracked }}
                        </td>

                    </tr>
                @empty
                    <tr>
                        <td>No entries</td>
                    </tr>
                @endforelse

                <tr>
                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500" colspan="2">
                        Total
                    </td>

                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ $transaction->where('amount', ">", 0)->sum('amount') }}
                    </td>

                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">
                        {{ $transaction->where('amount', "<", 0)->sum('amount') * -1}}
                    </td>

                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">

                    </td>
                </tr>

                {{--                <tr>--}}
                {{--                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500" colspan="2">--}}
                {{--                    </td>--}}
                {{--                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500" colspan="2">--}}
                {{--                        Difference--}}
                {{--                    </td>--}}

                {{--                    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500">--}}
                {{--                        {{ difference }}--}}
                {{--                    </td>--}}
                {{--                </tr>--}}

                </tbody>
            </table>

        </div>
    </x-container>


    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($journal->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
