<x-layout>
    <div class="container mx-auto">
        <div class="grid grid-cols-12">
            <div class="col-span-12 lg:col-span-12 grid grid-cols-12 col-gap-2">
                <div class="col-span-12 md:col-span-3 py-8">
                    <div class="m-4 p-8 bg-transparent shadow-md border border-gray-200 rounded-md">
                        <p class="text-gray-500 text-left text-xs">Month To Date</p>
                        <h5 class="font-extrabold">Income</h5>
                        <h2 class="text-primary font-extrabold text-4xl">{{ number_format($ytdIncome, 2) }}</h2>
                    </div>
                </div>

                <div class="col-span-12 md:col-span-3 py-8">
                    <div class="m-4 p-8 bg-transparent shadow-md border border-gray-200 rounded-md">
                        <p class="text-gray-500 text-left text-xs">Month To Date</p>
                        <h5 class="font-extrabold">Expense</h5>
                        <h2 class="text-primary font-extrabold text-4xl">{{ number_format($ytdExpenses, 2) }}</h2>
                    </div>
                </div>
                <div class="col-span-12 md:col-span-3 py-8">
                    <div class="m-4 p-8 bg-transparent shadow-md border border-gray-200 rounded-md relative">
                        <p class="text-gray-500 text-left text-xs">Month To Date</p>
                        <h5 class="font-extrabold">Cash</h5>
                        <h2 class="text-primary font-extrabold text-4xl">{{ number_format($cash, 2) }}</h2>
                        <div class="absolute bottom-1 right-1 cursor-pointer">
                            <a href="{{ url('cash-transactions/create') }}">
                                <i class="fas fa-plus-circle fa-2x text-primary hover:opacity-80 hover:scale-125 transition ease-in-out duration-150"></i>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="col-span-12 md:col-span-3 py-8">
                    <div class="m-4 p-8 bg-transparent shadow-md border border-gray-200 rounded-md">
                        <p class="text-gray-500 text-left text-xs">Month To Date</p>
                        <h5 class="font-extrabold">Stocks</h5>
                        <h2 class="text-primary font-extrabold text-4xl">{{ number_format($stocks, 2) }}</h2>
                    </div>
                </div>
            </div>

            <div class="col-span-12 lg:col-span-12 grid grid-cols-12 col-gap-2">

                <div class="col-span-12 md:col-span-6">
                    <div class="m-4 p-8 bg-transparent shadow-md border border-gray-200 rounded-md">
                        <p class="text-gray-500 text-left text-xs">As of {{ now()->format('M d, Y') }}</p>
                        <h5 class="font-extrabold">Expenses for reimbursement</h5>
                        <h2 class="text-primary font-extrabold text-4xl">{{ number_format($totalUnreconExpenses, 2) }}</h2>

                        <div class="col-span-12 mr-20 mt-4 text-xs">
                            <unreconciled-reimbursable :for-dashboard="true"/>

                            @if($unreconExpenses->count() > 1)
                                <button>Record Reimbursement</button>
                                <small>NOTE: transfer to vue. pag kaclick sa record button, dala lahat ng checked
                                    items, debit cash onhand/in bank</small>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-span-12 md:col-span-6">
                    <div class="m-4 p-8 bg-transparent shadow-md border border-gray-200 rounded-md">
                        <p class="text-gray-500 text-left text-xs">As of {{ now()->format('M d, Y') }}</p>
                        <h5 class="font-extrabold">Unrecorded bank transactions</h5>
                        <h2 class="text-primary font-extrabold text-4xl">{{ number_format($totalUnreconBankTrans, 2) }}</h2>

                        <div class="col-span-12 ml-4 mr-20 mt-4 text-sm font-bold">
                            @forelse ($unreconBankTrans as $key => $transaction)
                                <div class="flex justify-between mb-3">
                                    <p>{{ $key + 1 }}.
                                        <a href="{{ route('bank-transactions.journals.create', $transaction) }}">
                                            {{ ucfirst(strtolower($transaction->description)) }}
                                        </a>
                                        <br>
                                        <span
                                            class="text-xs font-thin ml-4">{{ $transaction->date->format("M d, Y") }}</span>
                                    </p>
                                    <p>{{ number_format(! is_null($transaction->debit) ? $transaction->debit : ($transaction->credit * -1), 2) }}</p>

                                </div>
                            @empty
                                <li>All good.</li>
                            @endforelse
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</x-layout>
