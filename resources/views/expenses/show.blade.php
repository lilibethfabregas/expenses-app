<x-layout>
    <x-page-header>
        <div class="my-1">
            <h5 class="text-xl font-thin text-primary">
                {{ ucfirst($expense->particulars) }}
            </h5>
            <p class="max-w-2xl text-xs text-gray-500 text-left">
                <a href="{{ route('expenses.index') }}" class="focus:outline-none"><i class="fas fa-arrow-left focus:outline-none"></i> Back to All Expenses</a>
            </p>
        </div>
    </x-page-header>

    <x-container>
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <div class="flex w-full justify-end items-center p-4">
                    <div class="flex space-x-4 relative">
{{--                        <a href="{{ route('expenses.edit', $expense) }}" class="flex flex-shrink-0 flex-grow-0 bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">--}}
{{--                            <div class="py-2 px-4 text-white font-bold">--}}
{{--                                Update--}}
{{--                            </div>--}}
{{--                            <div class="bg-white px-3 flex flex-shrink-0 py-3">--}}
{{--                                <i class="far fa-edit fa-lg text-primary"></i>--}}
{{--                            </div>--}}
{{--                        </a>--}}

                        <form action="{{ route('expenses.destroy', $expense) }}" method="POST">
                            @csrf @method('DELETE')
                            <button class="flex bg-primary rounded-xl overflow-hidden border border-primary shadow-lg focus:outline-none">
                                <div class="py-2 px-4 text-white font-bold">
                                    Delete
                                </div>
                                <div class="bg-white px-3 flex flex-shrink-0 py-3">
                                    <i class="far fa-trash-alt fa-lg text-primary"></i>
                                </div>
                            </button>
                        </form>
                    </div>
                </div>

                <div class="border-t border-gray-200">
                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                            <x-input
                                name="particulars"
                                placeholder=" "
                                label="Particulars"
                                width="w-11/12 p-2"
                                readonly="readonly"
                                value="{{ old('particulars') ? old('particulars') : $expense->journal->description }}"
                                error="{{ $errors->first('particulars') }}"
                            ></x-input>
                        </div>
                    </div>

                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                            <x-input
                                name="amount"
                                placeholder=" "
                                label="Amount"
                                width="w-11/12 p-2"
                                readonly="readonly"
                                value="{{ number_format($expense->journal->amount, 2) }}"
                                error="{{ $errors->first('amount') }}"
                            ></x-input>
                        </div>
                    </div>

                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                            <x-input
                                name="debit"
                                placeholder=" "
                                label="Expense Account"
                                width="w-11/12 p-2"
                                readonly="readonly"
                                value="{{ ucfirst($expense->journal->chartOfAccounts->name) }}"
                                error="{{ $errors->first('debit') }}"
                            ></x-input>
                        </div>
                    </div>

                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                            <x-input
                                name="credit"
                                placeholder=" "
                                label="Credit Account"
                                width="w-11/12 p-2"
                                value="{{ ucfirst($credit) }}"
                                readonly="readonly"
                                error="{{ $errors->first('credit') }}"
                            ></x-input>
                        </div>
                    </div>

                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                            <x-input
                                placeholder=" "
                                label="Category"
                                width="w-11/12 p-2"
                                value="{{ ucfirst($expense->category) }}"
                                readonly="readonly"
                            ></x-input>
                        </div>
                    </div>

                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
{{--                            <options--}}
{{--                                :choices="{{ strtolower($categories->pluck('name')) }}"--}}
{{--                                :route="'/categories'"--}}
{{--                                :label="'Category'"--}}
{{--                                :name="'category'"--}}
{{--                                :passed-value="'{{ old('category') ? old('category') : $expense->category }}'"--}}
{{--                            ></options>--}}
{{--                            @if ($errors->has('category'))--}}
{{--                                <small class="text-red-400 mx-2">{{ $errors->first('category') }}</small>--}}
{{--                            @endif--}}
                        </div>
                    </div>

                    <div class="p-4 grid grid-cols-3 gap-4">
                        <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                            <x-input
                                name="date"
                                placeholder=" "
                                label="Date"
                                width="w-11/12 p-2"
                                readonly="readonly"
                                value="{{ old('date') ? old('date') : $expense->journal->date->format('M d, Y') }}"
                                error="{{ $errors->first('date') }}"
                            ></x-input>
                        </div>
                    </div>

                    @if ($expense->is_joint && $expense->is_reconciled)

                        <div class="p-4 grid grid-cols-3 gap-4">
                            <div class="sm:mt-0 col-span-3 md:col-span-2 md:col-start-2 w-full">
                                <x-input
                                    placeholder=" "
                                    label="Reconciled Date"
                                    width="w-11/12 p-2"
                                    readonly="readonly"
                                    value="{{ $expense->reconciled_at->format('M d, Y') }}"
                                ></x-input>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
    </x-container>

    <x-container>
        {{--slot begins here --}}
        <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
            <tr>
                <th scope="col" colspan="3"
                    class="px-2 md:px-6 py-3 text-left text-gray-500 uppercase tracking-tighter text-sm font-bolder">
                    Activity Logs
                </th>
            </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
            @forelse ($expense->activities as $activity)
                @include('partials._activities-list')
            @empty
                <tr>
                    <td class="px-2 md:px-6 py-4 whitespace-nowrap">
                        <div class="flex items-center">
                            <div class="ml-4 text-xs">
                                No activities yet.
                            </div>
                        </div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
        {{--slot ends here --}}
    </x-container>
</x-layout>
