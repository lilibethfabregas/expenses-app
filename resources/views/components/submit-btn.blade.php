@props([
'submitText' => '',
])

<div class="mx-auto flex justify-end mt-4">
    <div class="rounded-md mx-1 lg:mx-0">
        <button type="submit" class="px-8 md:px-10 py-2 text-white bg-primary hover:bg-hover rounded-md">
            {{ $submitText ? $submitText : 'Submit' }}
        </button>
    </div>
</div>
