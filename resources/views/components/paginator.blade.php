@props([
    'count' => '',
    'hasNextPage' => '',
    'currentPage' => '',
    'nextPageUrl' => '',
    'prevPageUrl' => '',
    'model' => '',
])

<tr class="font-bolder text-left tracking-wider uppercase text-gray-700 text-sm">
    <td class="px-2 md:px-6 py-2 whitespace-nowrap">
        @if ($currentPage > 1)
            <a href="{{ $model }}?page=1" class="cursor-pointer focus:outline-none">
                <i class="fas fa-angle-double-left"></i>
            </a>
            <a href="{{ $prevPageUrl }}" class="cursor-pointer focus:outline-none">
                <i class="fas fa-angle-left"></i>
            </a>
        @endif
    </td>
    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-gray-500 hidden lg:block">
        {{ $currentPage  }} of
        <a href="{{ $model }}?page={{ceil($count / config('expenses.paginate_count'))}}">
            {{ ceil($count / config('expenses.paginate_count')) }}
        </a>
    </td>
    <td class="px-2 md:px-6 py-2 whitespace-nowrap text-right">
        @if ($hasNextPage)
            <a href="{{ $nextPageUrl }}" class="cursor-pointer focus:outline-none">
                <i class="fas fa-angle-right"></i>
            </a>
            <a href="{{ $model }}?page={{ceil($count / config('expenses.paginate_count'))}}" class="cursor-pointer focus:outline-none">
                <i class="fas fa-angle-double-right"></i>
            </a>
        @endif
    </td>
</tr>
