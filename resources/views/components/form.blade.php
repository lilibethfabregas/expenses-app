@props([
'method' => 'POST',
'action' => '',
])
<form action="{{ $action }}"
      method="{{ $method === 'GET' ? 'GET' : 'POST' }}">
    @csrf
    @if (! in_array($method, ['GET', 'POST']))
        @method($method)
    @endif
    <div class="shadow overflow-hidden sm:rounded-md">
        <div class="px-4 py-5 bg-white sm:p-6">
            {{ $slot }}
        </div>
    </div>
</form>
