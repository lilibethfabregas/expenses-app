<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!--Font Awesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('css_after')
    @yield('js_after')

</head>
<body class="font-sans bg-gray-50">
<div id="app" class="relative min-h-screen">
    @include('components.nav')
   <div class="max-w-screen-2xl mx-auto">
       {{ $slot }}
   </div>
    <div class="bg-gray-50 absolute -bottom-20 w-full h-50 overflow-hidden">
        <svg data-name="Layer 1"
             xmlns="http://www.w3.org/2000/svg"
             viewBox="0 0 1200 120" preserveAspectRatio="none">
            <path
                d="M985.66,92.83C906.67,72,823.78,31,743.84,14.19c-82.26-17.34-168.06-16.33-250.45.39-57.84,11.73-114,31.07-172,41.86A600.21,600.21,0,0,1,0,27.35V120H1200V95.8C1132.19,118.92,1055.71,111.31,985.66,92.83Z"
                class="shape-fill"
                fill="#21AFD6" fill-opacity="1"/>
        </svg>
    </div>
</div>
</body>

<script>
    function toggleMenu(event) {
        let navbar = document.getElementById('navbar')
        navbar.classList.toggle("hidden");
    }
</script>
</html>
