<div class="container mx-auto p-2 md:p-4">
    <div class="w-full mx-auto">
        <div class="flex flex-col">
            <div class="py-2 align-middle inline-block min-w-full sm:px-1">
                <div class="shadow overflow-hidden border-b border-gray-200 rounded-lg">

                    {{ $slot }}

                </div>
            </div>
        </div>
    </div>
</div>
