<div class="relative">
    <nav class="w-full flex items-center justify-between flex-wrap pt-6 md:p-6">
        <div class="flex items-center flex-shrink-0 text-gray-800 mr-6">
          <h1 class="text-3xl font-extrabold text-primary p-6 cursor-pointer focus:outline-none"><a href="/home">SPENDEE</a></h1>
        </div>

        <div class="block lg:hidden">
            <button class="flex items-center mr-6 px-6 py-2 border rounded text-teal-100 border-teal-400" id="navbar-btn" onclick="toggleMenu()">
                <svg class="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <title>Menu</title>
                    <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
                </svg>
            </button>
        </div>

        <div class="w-full hidden md:block flex-grow lg:flex lg:items-center lg:w-auto transform duration-500" id="navbar">
            <div class="text-sm sm:flex-grow md:space-x-6 md:flex md:justify-end text-primary bg-gray-200 md:bg-transparent font-semibold p-6">
                @guest
                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 border-2 border-white rounded-3xl">
                        <a class="block px-8 py-3 focus:outline-none"
                           href="{{ url('authenticate') }}">Login / Register</a>
                    </li>
                @else
                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans">
                        <p class="py-4"><a class="focus:outline-none"
                           href="{{ url('banks') }}">Banks</a></p>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans">
                        <p class="py-4"><a class="focus:outline-none"
                           href="{{ url('cash-transactions') }}">Cash</a></p>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans">
                        <p class="py-4"><a class="focus:outline-none"
                           href="{{ url('stocks') }}">Stocks</a></p>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 font-sans">
                        <p class="py-4"><a class="focus:outline-none"
                           href="{{ url('expenses') }}">Expenses</a></p>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 w-32 border-2 border-primary rounded-3xl">
                        <a class="block px-8 py-3 focus:outline-none"
                           href="{{ url('profile') }}">Settings</a>
                    </li>

                    <li class="no-underline block mt-4 sm:inline-block sm:mt-0 w-32 border-2 border-primary rounded-3xl">
                        <a class="block px-8 py-3 focus:outline-none" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        <form id="logout-form"
                              action="{{ action([\App\Http\Controllers\Auth\LoginController::class, 'logout']) }}"
                              method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                @endguest
            </div>
        </div>
    </nav>
</div>
