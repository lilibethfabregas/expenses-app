@props([
    'method' => 'POST',
    'action' => '',
])
 <form action="{{ $action }}"
      method="{{ $method === 'GET' ? 'GET' : 'POST' }}">
    @csrf
    @if (! in_array($method, ['GET', 'POST']))
        @method($method)
    @endif
{{--         {{ $slot }}--}}
     <div class="shadow-md px-4 py-2 border-gray-300 rounded-full flex justify-start items-center text-xs my-2 bg-white">
         <i class="fas fa-search"></i>
         <input type="text"
                name="q"
                placeholder="Search"
                class="mt-1 focus:outline-none focus:ring-none focus:border-blue-300 block bg-transparent mx-2">
     </div>
 </form>
