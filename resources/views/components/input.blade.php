@props([
'name' => '',
'placeholder' => '',
'value' => '',
'type' => '',
'class' => '',
'label' => '',
'width' => '',
'margin' => '',
'error' => '',
'readonly' => '',
])

<div class="{{ $width }} mt-6">
    <div class="relative p-2 border-b border-primary focus-within:border-b-2 {{ $margin }}">
        <input id="{{ $name }}"
               type="{{ $type ? $type : 'text' }}"
               name="{{ $name }}"
               value="{{ $value }}"
               {{ $readonly ? 'readonly' : '' }}
               class="block w-full appearance-none focus:outline-none bg-transparent text-black focus:ring-none"
               placeholder="{{ $placeholder }}"
        >
        <label for="{{ $name }}" class="absolute top-3 duration-300 text-gray-400">{{ $label }}</label>
    </div>
</div>

@if ($errors->has($name))
    <small class="text-red-400 mx-2">{{ $errors->first($name) }}</small>
@endif
