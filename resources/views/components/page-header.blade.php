<div class="container mx-auto px-4 pt-4 flex">
    <div class="w-full mx-auto">
        <div class="md:flex w-full justify-between items-center">
            {{ $slot }}
        </div>
    </div>
</div>

