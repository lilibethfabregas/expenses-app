<nav class="flex items-center justify-between flex-wrap pt-4">
    <div class="w-full flex-grow sm:flex sm:items-center sm:w-auto block">
        <div class="text-sm sm:flex-grow">
            <a href="/expenses"
               class="md:ml-16 no-underline block mt-4 sm:inline-block sm:mt-0 font-bold hover:text-gray-200 mr-4">
                Expenses
            </a>
            <a href="/banks"
               class="md:ml-16 no-underline block mt-4 sm:inline-block sm:mt-0 font-bold hover:text-gray-200 mr-4">
                Banks
            </a>
            <a href="/stocks"
               class="md:ml-16 no-underline block mt-4 sm:inline-block sm:mt-0 font-bold hover:text-gray-200 mr-4">
                Stocks
            </a>
        </div>
    </div>
</nav>
