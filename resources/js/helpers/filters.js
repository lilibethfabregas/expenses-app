import moment from 'moment'

export default {
  filters: {
    capitalize(value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    },
    currency(value) {
      let formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'PHP',
      });

      return formatter.format(value);
    },
    number(value) {
      return value.toFixed(2);
    },
    date: function (value) {
      return moment(value).format('DD MMMM YYYY')
    },
    shortDate: function (value) {
      return moment(value).format('D MMM YY')
    },
  },
}
