/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('text-input', require('./components/TextInput.vue').default);
Vue.component('select-input', require('./components/SelectInput.vue').default);

Vue.component('toggle-button', require('./components/Buttons/Toggle.vue').default);
Vue.component('toggle-update', require('./components/Buttons/ToggleUpdate.vue').default);
Vue.component('toggle-checkbox', require('./components/Buttons/ToggleCheckbox.vue').default);
Vue.component('options', require('./components/Options.vue').default);
Vue.component('options-without-autosave', require('./components/OptionsWithoutAutoSave.vue').default);
Vue.component('options-with-model-update', require('./components/OptionsWithModelUpdate.vue').default);
Vue.component('chart-of-accounts', require('./components/ChartOfAccounts.vue').default);
Vue.component('add-new-account', require('./components/AddNewAccount.vue').default);
Vue.component('add-new-broker', require('./components/AddNewBroker.vue').default);
Vue.component('for-reimbursement', require('./components/ForReimbursement.vue').default);
Vue.component('add-journal-entry', require('./components/AddJournalEntry.vue').default);

//PAGES
Vue.component('create-journal', require('./pages/CreateJournal.vue').default);
Vue.component('receive-cash', require('./pages/ReceiveCash.vue').default);
Vue.component('unreconciled-reimbursable', require('./pages/UnreconciledReimbursable.vue').default);

Vue.component('account-config', require('./pages/AccountConfig.vue').default);

Vue.component('authenticate', require('./pages/Authenticate.vue').default);
Vue.component('login', require('./pages/Login.vue').default);
Vue.component('register', require('./pages/Register.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


// import { gsap, ScrollTrigger, Draggable, MotionPathPlugin } from "gsap/all";
//
// // don't forget to register plugins
// gsap.registerPlugin(ScrollTrigger, Draggable, MotionPathPlugin);
//


const app = new Vue({
  el: '#app',
});

