<?php

namespace Database\Factories;

use App\Models\Bank;
use App\Models\BankTransaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BankTransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BankTransaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bank_id' => 1,
            'description' => 'Salary',
            'debit' => 0,
            'credit' => 10401.32,
            'balance' => 10401.32,
            'date' => 'Mar 2, 2021'
        ];
    }
}
