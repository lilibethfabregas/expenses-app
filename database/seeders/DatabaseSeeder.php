<?php

namespace Database\Seeders;

use App\Models\Bank;
use App\Models\Cash;
use App\Models\Category;
use App\Models\Classification;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(AbilitiesSeeder::class);
        $this->call(SuperAdminSeeder::class);
        $this->call(UsersSeeder::class);

        Category::factory()->create(['name' => 'food']);
        Category::factory()->create(['name' => 'transpo']);

        Classification::factory()->create(['name' => 'joint']);
        Classification::factory()->create(['name' => 'personal']);

        Cash::factory([
            'user_id' => 1,
            'name' => 'Cash on hand',
        ])->create();

        Bank::factory([
            'user_id' => 1,
            'name' => 'BDO',
            'account_number' => '00912722',
            'account_name' => 'Lilibeth Fabregas',
            'slug' => 'bdo',
        ])->create();

        Bank::factory([
            'user_id' => 1,
            'name' => 'BPI',
            'account_number' => '004990043',
            'account_name' => 'Lilibeth Fabregas',
            'slug' => 'bpi',
        ])->create();
    }
}
