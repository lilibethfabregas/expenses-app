<?php

namespace Database\Seeders;

use App\Models\Ability;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    protected $modelsDirectory = 'app/Models/*';

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->seedSuperAdminUsers();
    }

    private function seedSuperAdminUsers()
    {
        $user = User::query()->firstOrCreate(
            [
                'name' => 'Beth Fabregas',
                'slug' => 'beth-fabregas',
                'email' => 'superadmin@janthebuts.com',
                'email_verified_at' => now(),
                'password' => Hash::make('Hello123'),
                'role_id' => Role::SUPERADMIN,
            ],
        );

        $user->assignRole(Role::SUPERADMIN);

        $abilities = Ability::all();

        $abilities->each(function($ability) {
          $superAdmin = Role::find(Role::SUPERADMIN);

          $superAdmin->allowTo($ability);
        });
    }
}
