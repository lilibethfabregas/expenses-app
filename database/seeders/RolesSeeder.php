<?php

namespace Database\Seeders;

use App\Models\Ability;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->seedRoles();
    }

    private function seedRoles()
    {
        $roles = [
            'Super Admin',
            'Admin',
            'User'
        ];

        foreach ($roles as $role) {
            $role = Role::create([
                'name' => ucwords(strtolower($role)),
                'slug' => Str::slug($role)
            ]);

            $this->command->info('Added role: ' . $role->name);
        }
    }
}
