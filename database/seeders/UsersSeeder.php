<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->seedRoles();
    }

    private function seedRoles()
    {
        $roles = Role::query()->whereKeyNot(Role::SUPERADMIN)->get();

        $roles->each(function ($role) {
            User::factory(3)->create(['role_id' => $role->id]);
        });
    }
}
