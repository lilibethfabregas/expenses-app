<?php

namespace Database\Seeders;

use App\Models\Ability;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AbilitiesSeeder extends Seeder
{
    protected $modelsDirectory = 'app/Models/*';
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->seedGeneratedPermissions();
        $this->seedAdditionalPermissions();
    }

    /**
     * Get all the models in the app/Models folder, and generate permissions for those.
     */
    private function seedGeneratedPermissions()
    {
        $dir = base_path($this->modelsDirectory);
        $slugModels = [];
        foreach (glob($dir) as $file) {
            if (! is_dir($file)) {
                $slugModels[] = $this->studlyToSlug(basename($file));
            }
        }

        $resourceMethods = [
            'view-any',
            'view',
            'create',
            'delete',
            'update',
            'restore',
            'force-delete',
        ];

        foreach ($slugModels as $slugModel) {
            foreach ($resourceMethods as $resourceMethod) {
                Ability::query()->create([
                    'name' => ucfirst(str_replace('-', ' ', $resourceMethod). ' ' . str_replace('-', ' ', $slugModel)),
                    'slug' => Str::slug($resourceMethod) . '-' . Str::slug($slugModel),
                ]);
            }
        }
    }


    private function studlyToSlug($input)
    {
        $input = str_replace('.php', '', $input);

        $arr = preg_split(
            '/(^[^A-Z]+|[A-Z][^A-Z]+)/',
            $input,
            -1, /* no limit for replacement count */
            PREG_SPLIT_NO_EMPTY /*don't return empty elements*/
            | PREG_SPLIT_DELIM_CAPTURE /*don't strip anything from output array*/
        );

        return Str::slug(implode(' ', $arr));
    }

    private function seedAdditionalPermissions()
    {
        $permissions = [
            'view-any-application-log',
            'view-admin-panel',
            'view-hr-panel',
            'view-sales-panel',
            'view-accounting-panel',
            'view-applicant-panel',
        ];

        foreach ($permissions as $permission) {
            Ability::query()->create([
                'name' => ucfirst(str_replace('-', ' ', $permission)),
                'slug' => $permission]);
        }
    }

}
