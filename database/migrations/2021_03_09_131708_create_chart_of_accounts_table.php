<?php

use App\Models\ChartOfAccounts;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\SimpleExcel\SimpleExcelReader;

class CreateChartOfAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chart_of_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('number');
            $table->string('name');
            $table->string('classification');
            $table->timestamps();
        });

        SimpleExcelReader::create(__DIR__.'/../data/chart_of_accounts.csv')
            ->getRows()
            ->each(function (array $row) {

                ChartOfAccounts::query()->create([
                    'user_id' => 1,
                    'number' => $row['number'],
                    'name' => $row['name'],
                    'classification' => $row['classification'],
                ]);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chart_of_accounts');
    }
}
