<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('stock')->nullable();
            $table->integer('shares')->nullable();
            $table->float('price', '12', 4)->nullable();
            $table->float('charges', '12', 4)->nullable();
            $table->float('sales_tx', '12', 4)->nullable();
            $table->float('total', '12', 4);
            $table->unsignedBigInteger('broker_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('journal_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transactions');
    }
}
