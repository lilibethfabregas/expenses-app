<?php

use App\Models\Config;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('digit');
            $table->integer('asset_start');
            $table->integer('asset_end');
            $table->integer('liability_start');
            $table->integer('liability_end');
            $table->integer('equity_start');
            $table->integer('equity_end');
            $table->integer('income_start');
            $table->integer('income_end');
            $table->integer('expenses_start');
            $table->integer('expenses_end');
            $table->integer('cash_on_hand');
            $table->integer('cash_in_broker');
            $table->integer('inventory');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config');
    }
}
