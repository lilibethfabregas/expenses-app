<?php

return [

    'paginate_count' => env('EXPENSES_PAGINATE_COUNT', 10),

];
