<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class RoleControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create(['role_id' => Role::SUPERADMIN]);
    }

    public function testUnauthorizedUserCannotViewRoles()
    {
        $response = $this->get('/roles');

        $response->assertStatus(403);
    }

    public function testOnlyAuthorizedPersonCanViewRoles()
    {
        $response = $this->actingAs($this->user)->get('/roles');

        $response->assertStatus(200);
    }

    public function testUnauthenticatedUserCannotCreateARole()
    {
        $response = $this->post('/roles',);

        $response->assertRedirect('/');
    }

    public function testUnauthorizedUserCannotCreateARole()
    {
        $user = User::factory()->create(['role_id' => Role::ADMIN]);

        $this->actingAs($user)->post('/roles')
            ->assertRedirect('/');
    }

    public function testOnlyAuthorizedPersonCanCreateARole()
    {
        $this->withoutExceptionHandling();

        $role = Role::factory()
            ->raw([
                'name' => 'New role',
                'slug' => 'new-role'
            ]);

        $this->actingAs($this->user)->post('/roles', $role);

        $this->assertDatabaseHas('roles', $role);
    }

    public function testCreateRoleFlow()
    {
        $this->withoutExceptionHandling();

        $role = Role::factory()->make();

        $response = $this->actingAs($this->user)->post('/roles', $role->toArray());

        $response->assertOk();
    }

    public function testNameFieldIsRequired()
    {
        $role = Role::factory()
            ->raw([
                'name' => '',
                'slug' => ''
            ]);

        $response = $this->actingAs($this->user)->post('/roles', $role);

        $response->assertSessionHasErrors(['name']);
    }

    public function testNameMustBeUnique()
    {
        $role = Role::factory()
            ->make([
                'name' => 'Role',
                'slug' => 'role'
            ]);

        $this->actingAs($this->user)
            ->post('/roles', $role->toArray())
            ->assertSessionDoesntHaveErrors();

        $this->actingAs($this->user)
            ->post('/roles', $role->toArray())
            ->assertSessionHasErrors('name');

        $roleCount = Role::query()->where('name', $role->name)->count();

        $this->assertEquals(1, $roleCount);
    }

    public function testAuthorizedUserCanViewRoleWithAbilities()
    {
        $role = Role::factory()
            ->create([
                'name' => 'Role',
                'slug' => 'role'
            ]);

        $this->actingAs($this->user)->get('/roles/' . $role->slug)
            ->assertStatus(200)
            ->assertViewIs('roles.show')
            ->assertViewHas('abilities');
    }

    public function testUpdateRoleFlow()
    {
        $this->withoutExceptionHandling();

        $role = Role::factory()
            ->create([
                'name' => 'Librarian',
                'slug' => 'librarian'
            ]);

        $newRole = Role::factory()
            ->make([
                'name' => 'Architect',
                'slug' => 'architect'
            ]);

        $this->actingAs($this->user)
            ->put('/roles/' . $role->slug, $newRole->toArray())
            ->assertRedirect('/roles/' . $newRole->slug)
            ->assertSessionHas('flash', $newRole->name . ' has been updated.');

        $this->assertDatabaseHas('roles', $newRole->toArray());
        $this->assertDatabaseMissing('roles', $role->toArray());
    }

    public function testUpdateRoleWithAbilitiesFlow()
    {
        $this->withoutExceptionHandling();

        $role = Role::factory()
            ->create([
                'name' => 'Librarian',
                'slug' => 'librarian',
            ]);

        $this->actingAs($this->user)
            ->json('put', '/roles/' . $role->slug, [
                'name' => 'Librarian',
                'slug' => 'librarian',
                'ability' => 'view-any-user'
            ])
            ->assertRedirect('/roles/' . $role->slug);

        $this->assertEquals('view-any-user', $role->abilities->first()->slug);
    }

    public function testDeleteRoleFlow()
    {
        $this->withoutExceptionHandling();

        $role = Role::factory()
            ->create([
                'name' => 'Old Role',
                'slug' => 'old-role'
            ]);

        $this->actingAs($this->user)
            ->delete('/roles/' . $role->slug)
            ->assertRedirect('/roles')
            ->assertSessionHas('flash', $role->name . ' has been deleted.');

        $this->assertDeleted('roles', $role->toArray());
    }

}
