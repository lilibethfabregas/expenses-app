<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create(['role_id' => Role::SUPERADMIN]);
    }

    public function testUnauthorizedUserCannotViewUsers()
    {
        $response = $this->get('/users');

        $response->assertStatus(403);
    }

    public function testOnlyAuthorizedPersonCanViewUsers()
    {
        $this->withoutExceptionHandling();

        $this->actingAs($this->user)->get('/users')
            ->assertStatus(200);
    }

    public function testUnauthenticatedUserCannotCreateUser()
    {
        $this->post('/users')
            ->assertRedirect('/');
    }

    public function testUnauthorizedUserCannotCreateUser()
    {
        $user = User::factory()->create(['role_id' => 2]);

        $response = $this->actingAs($user)->post('/users', ['name' => 'Sample Name']);

        $response->assertRedirect('/');
    }

    public function testOnlyAuthorizedPersonCanCreateUser()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()
            ->make([
                'name' => 'New User',
                'slug' => 'new-user',
                'email' => 'new.user@hrapp.com',
                'role_id' => 2
            ]);

        $this->actingAs($this->user)
            ->json('post', '/users', $user->toArray())
            ->assertRedirect('/users/' . $user->slug)
            ->assertSessionHas('flash', $user->name . ' has been added.');

        $this->assertDatabaseHas('users', $user->toArray());
    }

    public function testNameAndRoleFieldsAreRequired()
    {
        $user = User::factory()
            ->raw([
                'name' => '',
                'email' => '',
                'role_id' => ''
            ]);

        $this->actingAs($this->user)
            ->post('/users', $user)
            ->assertSessionHasErrors(['name', 'role_id']);
    }

    public function testNameMustBeUnique()
    {
        $this->withExceptionHandling();
        $user = User::factory()
            ->make([
                'name' => 'User 88',
                'slug' => 'user-88',
                'email' => 'user88@hrapp.com',
                'role_id' => 2
            ]);

        $this->actingAs($this->user)
            ->post('/users', $user->toArray())
            ->assertSessionDoesntHaveErrors();

        $this->actingAs($this->user)
            ->post('/users', $user->toArray())
            ->assertSessionHasErrors('name');

        $nameCount = User::query()->where('name', $user->name)->count();

        $this->assertEquals(1, $nameCount);
    }

    public function testAuthorizedUserCanViewUser()
    {
        $user = User::factory()
            ->create([
                'name' => 'User 88',
                'slug' => 'user-88',
                'email' => 'user88@hrapp.com',
                'role_id' => 2
            ]);

        $this->actingAs($this->user)
            ->get('/users/' . $user->slug)
            ->assertStatus(200)
            ->assertViewIs('users.show')
            ->assertViewHas('user');
    }

    public function testUpdateUserFlow()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()
            ->create([
                'name' => 'User 88',
                'slug' => 'user-88',
                'email' => 'user88@hrapp.com',
                'role_id' => 2
            ]);

        $newUser = User::factory()
            ->make([
                'name' => 'User 88 Edited',
                'slug' => 'user-88-edited',
                'email' => 'user88edited@hrapp.com',
                'role_id' => 3
            ]);

        $name = str_replace(' ', '%20', $newUser->slug); //TODO observe

        $this->actingAs($this->user)
            ->put('/users/' . $user->slug, $newUser->toArray())
            ->assertRedirect('/users/' . $name)
            ->assertSessionHas('flash', $newUser->name . ' has been updated.');

        $this->assertDatabaseHas('users', $newUser->toArray());
        $this->assertDatabaseMissing('users', $user->toArray());
    }


    public function testEmailMustBeUnique()
    {
        $user1 = User::factory()
            ->create([
                'name' => 'User 1',
                'email' => 'user88@hrapp.com',
                'role_id' => 2
            ]);

        $user2 = User::factory()
            ->create([
                'name' => 'User 2',
                'email' => 'user89@hrapp.com',
                'role_id' => 2
            ]);

        $this->actingAs($this->user)
            ->put( route('users.update', $user1), [
                'name' => 'User 1',
                'email' => 'same.email@hrapp.com',
                'role_id' => 2,
            ])
            ->assertSessionDoesntHaveErrors();

        $this->actingAs($this->user)
            ->put( route('users.update', $user2), [
                'name' => 'User 2',
                'email' => 'same.email@hrapp.com',
                'role_id' => 2,
            ])
            ->assertSessionHasErrors('email');

        $emailCount = User::query()->where('email', 'same.email@hrapp.com')->count();
        $this->assertEquals(1, $emailCount);
    }

    public function testDeleteUserFlow()
    {
        $this->withoutExceptionHandling();

        $user = User::factory()
            ->create();

        $this->actingAs($this->user)
            ->delete('/users/' . $user->slug)
            ->assertRedirect('/users')
            ->assertSessionHas('flash', $user->name . ' has been deleted.');

        $this->assertDeleted('users', $user->toArray());
    }
}
