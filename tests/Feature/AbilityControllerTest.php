<?php

namespace Tests\Feature;

use App\Models\Ability;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AbilityControllerTest extends TestCase
{
    use DatabaseTransactions;

    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create(['role_id' => Role::SUPERADMIN]);
    }

    public function testUnauthorizedUserCannotViewAbilities()
    {
        $response = $this->get('/abilities');

        $response->assertStatus(403);
    }

    public function testOnlyAuthorizedPersonCanViewAbilities()
    {
        $this->withoutExceptionHandling();
        $response = $this->actingAs($this->user)->get('/abilities');

        $response->assertStatus(200);
    }

    public function testUnauthenticatedUserCannotCreateAbility()
    {
        $response = $this->post('/abilities',);

        $response->assertRedirect('/');
    }

    public function testUnauthorizedUserCannotCreateAnAbility()
    {
        $response = $this->post('/abilities',);

        $user = User::factory()->create(['role_id' => Role::ADMIN]);

        $response->assertRedirect('/');
    }

    public function testOnlyAuthorizedPersonCanCreateAnAbility()
    {
        $this->withoutExceptionHandling();

        $ability = Ability::factory()
            ->make([
                'name' => 'New ability',
                'slug' => 'new-ability'
            ]);

        $this->actingAs($this->user)
            ->post('/abilities', $ability->toArray())
            ->assertOk();

        $this->assertDatabaseHas('abilities', $ability->toArray());
    }

    public function testNameFieldIsRequired()
    {
        $ability = Ability::factory()
            ->raw([
                'name' => '',
                'slug' => ''
            ]);

        $this->actingAs($this->user)
            ->post('/abilities', $ability)
            ->assertSessionHasErrors(['name']);
    }

    public function testNameMustBeUnique()
    {
        $ability = Ability::factory()
            ->make([
                'name' => 'Ability',
                'slug' => 'ability'
            ]);

        $this->actingAs($this->user)
            ->post('/abilities', $ability->toArray())
            ->assertSessionDoesntHaveErrors();

        $this->actingAs($this->user)
            ->post('/abilities', $ability->toArray())
            ->assertSessionHasErrors('name');

        $abilitiesCount = Ability::query()->where('name', $ability->name)->count();

        $this->assertEquals(1, $abilitiesCount);
    }

    public function testAuthorizedUserCanViewAbility()
    {
        $ability = Ability::factory()
            ->create([
                'name' => 'Ability',
                'slug' => 'ability'
            ]);

        $this->actingAs($this->user)
            ->get('/abilities/' . $ability->slug)
            ->assertStatus(200)
            ->assertViewIs('abilities.show')
            ->assertViewHas('ability');
    }

    public function testUpdateAbilityFlow()
    {
        $this->withoutExceptionHandling();

        $ability = Ability::factory()
            ->create([
                'name' => 'View Reports',
                'slug' => 'view-reports'
            ]);

        $newAbility = Ability::factory()
            ->make([
                'name' => 'View edited reports',
                'slug' => 'view-edited-reports'
            ]);

        $this->actingAs($this->user)
            ->put('/abilities/' . $ability->slug, $newAbility->toArray())
            ->assertRedirect('/abilities/' . $newAbility->slug)
            ->assertSessionHas('flash', $newAbility->name . ' has been updated.');

        $this->assertDatabaseHas('abilities', $newAbility->toArray());
        $this->assertDatabaseMissing('abilities', $ability->toArray());
    }

    public function testDeleteAbilityFlow()
    {
        $this->withoutExceptionHandling();

        $ability = Ability::factory()
            ->create([
                'name' => 'Old Ability',
                'slug' => 'old-ability'
            ]);

        $response = $this->actingAs($this->user)
            ->delete('/abilities/' . $ability->slug);

        $this->assertDeleted('abilities', $ability->toArray());

        $response->assertRedirect('/abilities')
            ->assertSessionHas('flash', $ability->name . ' has been deleted.');
    }
}
