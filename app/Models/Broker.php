<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Broker extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function stockTransaction()
    {
        return $this->hasMany(StockTransaction::class);
    }
}
