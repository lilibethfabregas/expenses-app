<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ability extends Model
{
    use HasFactory, SoftDeletes, RecordsActivity;

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function addAbilities($model)
    {
        $resourceMethods = collect([
            'view-any',
            'view',
            'create',
            'delete',
            'update',
            'restore',
            'force-delete',
        ]);

        $resourceMethods->each(function($action) use ($model) {
            Ability::create([
                'name' => ucfirst(str_replace('-', ' ', $action)) . ' ' . str_replace('-', ' ', $model),
                'slug' => $action . '-' . $model,
            ]);
        });
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }
}
