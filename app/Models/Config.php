<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    use HasFactory;

    protected $table = 'config';

    protected $fillable = [
        'user_id',
        'digit',
        'asset_start',
        'asset_end',
        'liability_start',
        'liability_end',
        'equity_start',
        'equity_end',
        'income_start',
        'income_end',
        'expenses_start',
        'expenses_end',
        'cash_on_hand',
        'cash_in_broker',
        'inventory',
    ];


}
