<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SpinWheelPrizeUser.
 *
 * @property bool $is_recorded
 */
class BankTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'bank_id',
        'description',
        'debit',
        'credit',
        'balance',
        'date',
        'recorded_at',
    ];

    protected $casts = [
        'date' => 'datetime',
    ];

    public function getIsRecordedAttribute()
    {
        return ! is_null($this->recorded_at);
    }

    public function journal()
    {
        return $this->hasMany(Journal::class, 'subject_id');
    }
}
