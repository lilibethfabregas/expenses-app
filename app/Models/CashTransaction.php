<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashTransaction extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'journal_id',
        'description',
        'debit',
        'credit',
        'balance',
        'date',
        'recorded_at',
    ];

    protected $casts = [
        'date' => 'datetime',
    ];

    public function getIsRecordedAttribute()
    {
        return ! is_null($this->recorded_at);
    }

    public function journal()
    {
        return $this->belongsTo(Journal::class);
    }
}
