<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Expense.
 *
 * @property bool $is_reconciled
 */
class Expense extends Model
{
    use HasFactory, SoftDeletes, RecordsActivity;

    protected $guarded = [];

    protected $casts= [
      'reconciled_at' => 'datetime',
      'is_joint' => 'boolean',
      'is_split' => 'boolean',
    ];

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function journal()
    {
        return $this->belongsTo(Journal::class);
    }

    public function getIsReconciledAttribute()
    {
        return ! is_null($this->reconciled_at);
    }

    public function scopeUnreconciled(Builder $query)
    {
        return $query->whereNull('reconciled_at');
    }
}
