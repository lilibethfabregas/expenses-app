<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'date',
        'reference',
        'description',
        'account',
        'amount',
        'is_tracked',
        'subject_id',
        'subject_type',
    ];

    protected $casts = [
        'date' => 'datetime',
        'is_tracked' => 'boolean',
    ];

    public function subject()
    {
        return $this->morphTo();
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function expense()
    {
        return $this->hasOne(Expense::class);
    }

    public function income()
    {
        return $this->hasOne(Income::class);
    }

    public function cashTransaction()
    {
        return $this->hasOne(CashTransaction::class);
    }

    public function stockTransaction()
    {
        return $this->hasOne(StockTransaction::class);
    }

//see if we can delete this
//    public function stock()
//    {
//        return $this->hasOne(Stock::class);
//    }

    public function chartOfAccounts()
    {
        return $this->belongsTo(ChartOfAccounts::class, 'account', 'number');
    }

    public function thirdParty()
    {
        return $this->belongsTo(ThirdParty::class);
    }

    public function scopeYtdIncome(Builder $query)
    {
        /**@var User $user**/
        $user = auth('web')->user();

        if ($user->config === null) {
          return 0;
        }

        return $query
            ->whereBetween('account', [$user->config->income_start, $user->config->income_end])
            ->sum('amount') * -1;
    }

    public function scopeYtdExpenses(Builder $query)
    {
        /**@var User $user**/
        $user = auth('web')->user();

        if ($user->config === null) {
            return 0;
        }

        return $query
            ->whereBetween('account', [$user->config->expenses_start, $user->config->expenses_end])
            ->sum('amount');
    }

    public function scopeYtdCash(Builder $query)
    {
        /**@var User $user**/
        $user = auth('web')->user();

        if ($user->config === null) {
            return 0;
        }

        return $query
            ->where('account', $user->config->cash_on_hand)
            ->orWhere('account', '100101')
            ->sum('amount');
    }

    public function scopeYtdStocks(Builder $query)
    {
        /**@var User $user**/
        $user = auth('web')->user();

        if ($user->config === null) {
            return 0;
        }

        return $query
            ->where('account', $user->config->cash_in_broker)
            ->sum('amount');
    }

    public function unreconExpenses(Builder $query)
    {
        return $this->scopeYtdExpenses()->expenses()->where('reconciled_at', null)->get();
    }

    public function reverse()
    {
        $this->create([
            'user_id' => $this->user_id,
            'date' => now(),
            'reference' => 'reversal: ' . $this->reference,
            'description' => 'reversal of ID: ' . $this->id,
            'account' => $this->account,
            'amount' => $this->amount * -1,
            'is_tracked' => $this->is_tracked,
            'subject_id' => $this->subject_id,
            'subject_type' => $this->subject_type,
        ]);
    }
}
