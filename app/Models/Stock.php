<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'journal_id',
        'broker_id'
    ];

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function journal()
    {
        return $this->belongsTo(Journal::class);
    }

    public function broker()
    {
        return $this->belongsTo(Broker::class, 'broker_id');
    }
}
