<?php

namespace App\Models;

trait RecordsActivity
{
    protected static function bootRecordsActivity()
    {
        if (auth()->guest()) return;

        foreach (static::getRecordEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event);
            });
        }
    }

    protected static function getRecordEvents()
    {
        return ['created', 'deleted', 'updated'];
    }

    protected function recordActivity($event)
    {
        $this->activity()->create([
            'event' => $event,
            'user_id' => auth()->id(),
        ]);
    }

    public function activity()
    {
        return $this->morphMany(Activity::class, 'subject');
    }
}
