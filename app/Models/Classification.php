<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classification extends Model
{
    use HasFactory, SoftDeletes, RecordsActivity;

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class,'classification');
    }
}
