<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use HasFactory, SoftDeletes, RecordsActivity;

    protected $guarded = [];

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function transactions()
    {
        return $this->hasMany(BankTransaction::class);
    }

    public function balance()
    {
        if ($this->transactions()->exists()) {
            return $this->transactions()->latest()->first()->balance;
        } else {
            return 0;
        }
    }

    public function scopeDebit()
    {
        return $this->transactions()->whereNull('recorded_at')->sum('debit');
    }
}
