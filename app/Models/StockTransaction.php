<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StockTransaction
 * @package App\Models
 * @method static Builder|StockTransaction currentShares()
 */
class StockTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'stock',
        'shares',
        'price',
        'total',
        'charges',
        'sales_tx',
        'broker_id',
        'user_id',
        'journal_id',
    ];

    public function scopeCurrentShares(Builder $query, $stock, $broker): int
    {
        return (int)$query
            ->where('stock', $stock)
            ->where('broker_id', $broker)
            ->sum('shares');
    }

    public function record($total, $journalId)
    {
        $this->create([
            'type' => 'DE',
            'stock' => null,
            'shares' => null,
            'price' => null,
            'total' => $total,
            'broker_id' => null, //magkkaron during update
            'journal_id' => $journalId,
            'user_id' => auth('web')->id(),
            'charges' => null,
            'sales_tx' => null,
        ]);
    }

    public function journal()
    {
        return $this->belongsTo(Journal::class);
    }

    public function broker()
    {
        return $this->belongsTo(Broker::class);
    }
}
