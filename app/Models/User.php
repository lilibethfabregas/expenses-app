<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Models\User.
 * @property-read Collection|Role[] $roles
 * @property-read Collection|Role[] $role
 * @property-read Collection|Ability[] $ability
 * @property-read Collection|Activity[] $activity
 * @property-read Collection|Category[] $categories
 * @property-read Collection|Classification[] $classifications
 * @property-read Collection|Broker[] $brokers
 * @property-read Collection|Bank[] $bank
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'password',
        'role_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            $user->assignRole($user->role_id);
        });

        static::updated(function ($user) {
            $user->assignRole($user->role_id);
        });
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function assignRole($role)
    {
        $this->roles()->sync($role);
    }

    public function abilities()
    {
        return $this->roles->map->abilities->flatten()->pluck('slug')->unique();
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    //this is for expenses: joint or personal
    public function classifications()
    {
        return $this->hasMany(Classification::class);
    }

    public function banks()
    {
        return $this->hasMany(Bank::class);
    }

    public function chartOfAccounts()
    {
        return $this->hasMany(ChartOfAccounts::class);
    }

    public function filteredChartOfAccounts($range = [100000, 199999])
    {
        return $this->chartOfAccounts()->whereBetween('number', $range);
    }

    public function journals()
    {
        return $this->hasMany(Journal::class);
    }

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    public function brokers()
    {
        return $this->hasMany(Broker::class);
    }

    public function stockTransactions()
    {
        return $this->hasMany(StockTransaction::class);
    }

    public function bankTransactions()
    {
        return $this->hasManyThrough(BankTransaction::class, Bank::class);
    }

    public function thirdParties()
    {
        return $this->hasMany(ThirdParty::class);
    }

    public function config()
    {
        return $this->hasOne(Config::class);
    }
}
