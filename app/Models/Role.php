<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes, RecordsActivity;

    const SUPERADMIN = 1;
    const ADMIN = 2;
    const USER = 3;

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function abilities()
    {
        return $this->belongsToMany(Ability::class)->withTimestamps();
    }

    public function allowTo($ability)
    {
        if (is_string($ability)) {
            $ability = Ability::query()->where('slug', $ability)->firstOrFail();
        }

        $this->abilities()->sync($ability, false);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function activities()
    {
        return $this->morphMany(Activity::class, 'subject');
    }
}
