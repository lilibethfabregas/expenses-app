<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class AccountMustNotOverlap implements Rule
{
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $assetStart = $this->request->get('assetStart');
        $assetEnd = $this->request->get('assetEnd');
        $liabilityStart = $this->request->get('liabilityStart');
        $liabilityEnd = $this->request->get('liabilityEnd');
        $equityStart = $this->request->get('equityStart');
        $equityEnd = $this->request->get('equityEnd');
        $incomeStart = $this->request->get('incomeStart');
        $incomeEnd = $this->request->get('incomeEnd');
        $expenseStart = $this->request->get('expensesStart');
        $expenseEnd = $this->request->get('expensesEnd');

        $account = Str::contains($attribute, 'Start') ?
            Str::replaceFirst('Start', '', $attribute) :
            Str::replaceFirst('End', '', $attribute);

        switch ($account) {
            case ($account === 'asset'):
                $overlap = $value >= $liabilityStart && $value <= $liabilityEnd ||
                    $value >= $equityStart && $value <= $equityEnd ||
                    $value >= $incomeStart && $value <= $incomeEnd ||
                    $value >= $expenseStart && $value <= $expenseEnd;

                return $overlap ? false : true;

            case ($account === 'liability'):
                $overlap = $value >= $assetStart && $value <= $assetEnd ||
                    $value >= $equityStart && $value <= $equityEnd ||
                    $value >= $incomeStart && $value <= $incomeEnd ||
                    $value >= $expenseStart && $value <= $expenseEnd;

                return $overlap ? false : true;

            case ($account === 'equity'):
                $overlap = $value >= $assetStart && $value <= $assetEnd ||
                    $value >= $liabilityStart && $value <= $liabilityEnd ||
                    $value >= $incomeStart && $value <= $incomeEnd ||
                    $value >= $expenseStart && $value <= $expenseEnd;

                return $overlap ? false : true;

            case ($account === 'income'):
                $overlap = $value >= $assetStart && $value <= $assetEnd ||
                    $value >= $liabilityStart && $value <= $liabilityEnd ||
                    $value >= $equityStart && $value <= $equityEnd ||
                    $value >= $expenseStart && $value <= $expenseEnd;

                return $overlap ? false : true;

            case ($account === 'expense'):
                $overlap = $value >= $assetStart && $value <= $assetEnd ||
                    $value >= $liabilityStart && $value <= $liabilityEnd ||
                    $value >= $equityStart && $value <= $equityEnd ||
                    $value >= $incomeStart && $value <= $incomeEnd;

                return $overlap ? false : true;

            default:
                return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The account must overlap with the other accounts.';
    }
}
