<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CoaClassificationMustBeValid implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $classifications = ['asset', 'liability', 'expenses', 'income', 'equity'];

        return in_array($value, $classifications);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The classification must be valid.';
    }
}
