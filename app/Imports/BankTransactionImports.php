<?php

namespace App\Imports;

use App\Models\BankTransaction;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

/**
 * Class BankTransactionImports
 * @package App\Imports
 */
class BankTransactionImports implements ToCollection
{
    public $bankId;

    public function __construct($bankId)
    {
        $this->bankId = $bankId;
    }
    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            BankTransaction::create([
                'bank_id' => $this->bankId,
                'description' => $row[1],
                'debit' => $row[6], //bank credit => salaries
                'credit' => $row[4], //bank debits => withdraw
                'balance' => $row[8],
                'date' => $row[0],
            ]);
        }
    }
}
