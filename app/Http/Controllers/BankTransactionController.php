<?php

namespace App\Http\Controllers;

use App\Imports\BankTransactionImports;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class BankTransactionController extends Controller
{
    public function __invoke(Request $request): RedirectResponse
    {
        $path = Storage::putFileAs(
            'upload',
            request()->file('file'),
            request()->file('file')->getClientOriginalName());

        try {

            Excel::import(new BankTransactionImports($request->bank_id), $path);
            return back()->with('message', 'Bank Transaction Import failed. Error!');

        } catch (\Exception $e) {

            Log::error('Bank Transaction Import failed. Error: ' . $e);
            return back()->withErrors(['message', 'Bank Transaction Import failed. Error: ' . $e], 'message');
        }
    }
}
