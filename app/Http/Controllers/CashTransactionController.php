<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreCashRequest;
use App\Http\Requests\UpdateCashRequest;
use App\Models\Activity;
use App\Models\Cash;
use App\Models\CashTransaction;
use App\Models\ChartOfAccounts;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CashTransactionController extends Controller
{
    public function index()
    {
        $cashTransactions = CashTransaction::withTrashed()->latest()->get();

        $activities = Activity::query()->where('subject_type', 'App\Models\CashTransaction')->limit(10)->latest()->get();

        return view('cash-transaction.index', compact('cashTransactions', 'activities'));
    }

//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return Application|Factory|View|Response
//     * @throws AuthorizationException
//     */
//    public function create()
//    {
//        $this->authorize('create-cash-transaction');
//
//        return view('cash-transaction.create');
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function receive()
    {
        $accounts = ChartOfAccounts::all();

        return view('cash-transaction.receive', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        $accounts = ChartOfAccounts::all();

        return view('cash-transaction.spend', compact('accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCashRequest $request
     * @return RedirectResponse
     */
    public function store(StoreCashRequest $request)
    {
        $cashTransaction = CashTransaction::create([
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $cashTransaction)
            ->with('flash', $cashTransaction->name . ' has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param CashTransaction $cashTransaction
     * @return Application|Factory|View|Response
     */
    public function show(CashTransaction $cashTransaction)
    {
        return view('cash-transaction.show', compact('cashTransaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Cash $cashTransaction
     * @return Application|Factory|View|Response
     */
    public function edit(Cash $cashTransaction)
    {
        return view('cash-transaction.edit', compact('cash-transaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCashRequest $request
     * @param Cash $cashTransaction
     * @return RedirectResponse
     */
    public function update(UpdateCashRequest $request, Cash $cashTransaction)
    {
        $cashTransaction->update([
            'name' => ucfirst(strtolower(str_replace('-', ' ', $request->name))),
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $cashTransaction)
            ->with('flash', $cashTransaction->name . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cash $cashTransaction
     * @return RedirectResponse
     */
    public function destroy(Cash $cashTransaction)
    {
        return redirect()->action([self::class, 'index'])
            ->with('flash', $cashTransaction->name . ' deleted.');
    }

    public function restore($cashTransactionSlug)
    {
        $retrieve = CashTransaction::onlyTrashed()->where('slug', $cashTransactionSlug)->first();
        $retrieve->restore();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $retrieve->name . ' has been restored.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $cashTransaction = CashTransaction::query()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->get();

        $archived = CashTransaction::onlyTrashed()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->get();

        $all = $cashTransaction->merge($archived);

        session()->flash(
            'flash',
            $all->count() . ' ' . Str::plural('result', $all->count()) . '  found.'
        );

        return view('cash-transaction.index', compact('cash-transaction', 'archived'));
    }
}
