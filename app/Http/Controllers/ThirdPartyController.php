<?php

namespace App\Http\Controllers;

use App\Models\ThirdParty;
use Illuminate\Http\Request;

class ThirdPartyController extends Controller
{
    public function store(Request $request)
    {
        $this->authorize('create-third-party');

        ThirdParty::create([
            'name' => ucfirst(strtolower($request->name)),
            'account_number' => $request->account_number,
            'details' => $request->details,
        ]);

        return response()->json(['message' => 'Third party added'], 200);
    }
}
