<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreBankRequest;
use App\Http\Requests\UpdateBankRequest;
use App\Models\Bank;
use App\Models\Activity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class BankController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $user = auth('web')->user();

        $banks = $user->banks;

        $activities = Activity::query()->where('subject_type', 'App\Models\Bank')->paginate(config('expenses.paginate_count'));

        return view('banks.index', compact('banks', 'activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBankRequest $request
     * @return RedirectResponse
     */
    public function store(StoreBankRequest $request)
    {
        $bank = Bank::create([
            'user_id' => auth('web')->id(),
            'name' => ucfirst(strtolower($request->name)),
            'account_number' => $request->account_number,
            'account_name' => $request->account_name,
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $bank)
            ->with('flash', $bank->name . ' has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param Bank $bank
     * @return Application|Factory|View|Response
     */
    public function show(Bank $bank)
    {
        return view('banks.show', compact('bank'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Bank $bank
     * @return Application|Factory|View|Response
     */
    public function edit(Bank $bank)
    {
        return view('banks.edit', compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBankRequest $request
     * @param Bank $bank
     * @return RedirectResponse
     */
    public function update(UpdateBankRequest $request, Bank $bank)
    {
        $bank->update([
            'name' => ucfirst(strtolower($request->name)),
            'account_number' => $request->account_number,
            'account_name' => $request->account_name,
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $bank)
            ->with('flash', $bank->name . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bank $bank
     * @return RedirectResponse
     */
    public function destroy(Bank $bank)
    {
        return redirect()->action([self::class, 'index'])
            ->with('flash', $bank->name . ' deleted.');
    }

    public function restore($bankSlug)
    {
        $retrieve = Bank::onlyTrashed()->where('slug', $bankSlug)->first();
        $retrieve->restore();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $retrieve->name . ' has been restored.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $banks = Bank::query()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->get();

        $archived = Bank::onlyTrashed()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->get();

        $all = $banks->merge($archived);

        session()->flash(
            'flash',
            $all->count() . ' ' . Str::plural('result', $all->count()) . '  found.'
        );

        return view('banks.index', compact('banks', 'archived'));
    }
}
