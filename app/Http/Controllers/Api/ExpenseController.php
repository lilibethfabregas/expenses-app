<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Transformers\ExpenseTransformer;
use App\Http\Transformers\ExpenseChartTransformer;
use App\Models\Expense;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param $modelId
     * @return JsonResponse
     */
    public function update($modelId)
    {
        $model = Expense::findOrFail($modelId);

        $model->update([
            'is_joint' => !$model->is_joint,
        ]);

        return response()->json(['status' => $model->is_joint], 200);
    }

    public function getUnreconciledReimbursable()
    {
        $user = auth('web')->user();

        $data = $user->expenses()->where('is_joint', true)->where('reconciled_at', null)->get();

        return $this->response->withCollection($data, new ExpenseTransformer());
    }
//    public function restore($id)
//    {
//        $model = Category::onlyTrashed()->where('id', $id)->first();
//        $model->restore();
//
//        return response()->json(['name' => $model->name], '200');
//    }

    public function chart()
    {
        $expenses = Expense::query()->get();

        return $this->response->withCollection($expenses, new ExpenseChartTransformer());
    }
}
