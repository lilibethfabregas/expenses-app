<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreConfigRequest;
use App\Http\Controllers\Controller;
use App\Models\Config;

class ConfigController extends Controller
{
    public function store(StoreConfigRequest $request)
    {
        Config::updateOrCreate([
            'user_id' => auth('web')->id(),
        ],[
            'user_id' => auth('web')->id(),
            'digit' => $request->digit,
            'asset_start' => $request->assetStart,
            'asset_end' => $request->assetEnd,
            'liability_start' => $request->liabilityStart,
            'liability_end' => $request->liabilityEnd,
            'equity_start' => $request->equityStart,
            'equity_end' => $request->equityEnd,
            'income_start' => $request->incomeStart,
            'income_end' => $request->incomeEnd,
            'expenses_start' => $request->expensesStart,
            'expenses_end' => $request->expensesEnd,
            'cash_on_hand' => $request->cashOnHand,
            'cash_in_broker' => $request->cashInBroker,
            'inventory' => $request->inventory,
        ]);

        return response()->json(['message' => 'Config saved.'], '200');
    }
}
