<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreJournalRequest;
use App\Models\CashTransaction;
use App\Models\Journal;
use App\Models\StockTransaction;
use App\Models\User;
use Carbon\Carbon;

class JournalController extends Controller
{
    public function store(StoreJournalRequest $request)
    {
        /**@var User $user * */
        $user = auth('web')->user();

        if ($request->id > 0) {
            $model = $request->type;

            $modelTransaction = $model::findOrFail($request->id);

            if ($modelTransaction->journal->count() > 0) {
                return response()->json(['message' => 'This transaction is already recorded.']);
            };
        }

        $entries = collect($request->entries);

        $debits = $entries->reduce(function ($carry, $item) {
            return $carry + $item['debit'];
        });

        $credits = $entries->reduce(function ($carry, $item) {
            return $carry + $item['credit'];
        });

        if (((int)$credits - (int)$debits) !== 0) {
            return response()->json(['message' => 'Not balance']);
        }

        if ($user->config === null) {
            return response()->json(['message' => 'Please setup chart of accounts first.']);
        }

        $entries->each(function ($entry) use ($request, $user) {
            $journal = Journal::create([
                'user_id' => auth('web')->id(),
                'date' => Carbon::parse($request->date)->format('M j, Y'),
                'reference' => $request->reference,
                'description' => $entry['description'],
                'amount' => is_null($entry['debit']) || $entry['debit'] === 0 ? $entry['credit'] * -1 : $entry['debit'],
                'account' => $entry['account'],
                'is_tracked' => is_null($entry['isTracked']) ? false : $entry['isTracked'],
                'subject_id' => $request->id,
                'subject_type' => $request->type
            ]);

            switch ($entry['account']) {
                case ($entry['account'] === $user->config->cash_on_hand):

                    $lastTransaction = CashTransaction::query()->latest()->first();
                    $changeInBalance = is_null($entry['debit']) || $entry['debit'] === 0 ? $entry['credit'] * -1 : $entry['debit'];

                    $journal->cashTransaction()->create([
                        'user_id' => $user->id,
                        'debit' => $entry['debit'],
                        'credit' => $entry['credit'],
                        'balance' => $lastTransaction ? $lastTransaction->balance + $changeInBalance : $changeInBalance,
                        'date' => Carbon::parse($request->date)->format('M j, Y'),
                    ]);

                    break;

                case ($entry['account'] === $user->config->cash_in_broker):
                    $amount = $entry['debit'] ? $entry['debit'] : $entry['credit'];

                    $user->brokers->create([

                    ]);
                    $stockTransaction = new StockTransaction();
                    $stockTransaction->record($amount, $journal->id);

                    break;

                case ($entry['account'] >= $user->config->liability_start && $entry['account'] <= $user->config->liability_end):
                    dump('liab ito');
                    break;
                case ($entry['account'] >= $user->config->equity_start && $entry['account'] <= $user->config->equity_end):
                    dump('equity ito');
                    break;
                case ($entry['account'] >= $user->config->income_start && $entry['account'] <= $user->config->income_end):
                    $journal->income()->create(['user_id' => $user->id]);
                    break;
                case ($entry['account'] >= $user->config->expenses_start && $entry['account'] <= $user->config->expenses_end):
                    $journal->expense()->create(['user_id' => $user->id]);
                    break;
                default:
                    dump('huh?');
                    break;
            }
        });

        if ($request->id > 0) {
            $modelTransaction->update([
                'recorded_at' => now(),
            ]);
        }

        return response()->json(['message' => 'Entry added.'], '200');
    }
}
