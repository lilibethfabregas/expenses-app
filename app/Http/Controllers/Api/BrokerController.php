<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBrokerRequest;
use App\Http\Requests\StoreChartOfAccountRequest;
use App\Models\Broker;
use App\Models\ChartOfAccounts;
use App\Models\Classification;
use Illuminate\Support\Str;

class BrokerController extends Controller
{
    public function store(StoreBrokerRequest $request)
    {
        $model = Broker::create([
            'user_id' => auth('web')->id(),
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'account_number' => $request->account_number
        ]);

        return response()->json(['broker' => $model], '200');
    }
}
