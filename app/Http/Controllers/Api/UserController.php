<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->delete();
    }


    public function restore($userSlug)
    {
        $retrieve = User::onlyTrashed()->where('slug', $userSlug)->first();
        $retrieve->restore();
    }
}
