<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class ThirdPartyController extends Controller
{
    public function index()
    {
        $user = auth('web')->user();

        return response()->json(['choices' => $user->thirdParties], 200);
    }

    public function store(Request $request)
    {
        //$this->authorize('create-third-party');
        $user = auth('web')->user();

        $user->thirdParties()->create([
            'name' => ucfirst(strtolower($request->name)),
            'account_number' => $request->account_number,
            'details' => $request->details,
        ]);

        return response()->json(['message' => ucfirst($request->name) . ' has been added.'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $model = Category::find($id);

        $model->delete();
    }

    public function restore($id)
    {
        $model = Category::onlyTrashed()->where('id', $id)->first();
        $model->restore();

        return response()->json(['name' => $model->name], 200);
    }
}
