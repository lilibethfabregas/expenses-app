<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCashTransactionRequest;
use App\Models\BankTransaction;
use App\Models\CashTransaction;
use App\Models\ChartOfAccounts;
use App\Models\Expense;
use App\Models\Journal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ReceiveCashJournalController extends Controller
{
    public function storeCashTransaction(StoreCashTransactionRequest $request)
    {
        /**@var User $user * */
        $user = auth('web')->user();

        $entries = collect($request->credits);

        $credits = $entries->reduce(function ($carry, $item) {
            return $carry + $item['amount'];
        });

        if (((int)$credits - (int)$request->debit) !== 0) {
            return response()->json(['message' => 'Not balance']);
        }

        if ($user->config === null) {
            return response()->json(['message' => 'Please setup chart of accounts first.']);
        }

        //create debit entry
        $journal = Journal::create([
            'user_id' => $user->id,
            'date' => Carbon::parse($request->date)->format('M j, Y'),
            'reference' => $request->reference,
            'description' => 'Reconciled joint expenses as of ' . now()->format('M d, Y'),
            'amount' => (int)$request->debit,
            'account' => 100100, //TODO create set up page for designated accounts
            'is_tracked' => false,
            'subject_id' => null,
            'subject_type' => $request->type
        ]);

        $lastTransaction = CashTransaction::query()->latest()->first();

        //create cash transactions
        $journal->cashTransaction()->create([
            'user_id' => $user->id,
            'debit' => (int)$request->debit >= 1 ? (int)$request->debit : 0,
            'credit' => (int)$request->debit < 1 ? (int)$request->debit : 0,
            'balance' => $lastTransaction ? $lastTransaction->balance + (int)$request->debit : (int)$request->debit,
            'date' => Carbon::parse($request->date)->format('M j, Y'),
        ]);

        //create credit entries
        $entries->each(function ($entry) use ($request, $user) {
            Journal::create([
                'user_id' => $user->id,
                'date' => Carbon::parse($request->date)->format('M j, Y'),
                'reference' => $request->reference,
                'description' => $entry['description'],
                'amount' => $entry['amount'] * -1,
                'account' => $entry['accountNumber'],
                'is_tracked' => is_null($entry['isTracked']) ? false : $entry['isTracked'],
                'subject_id' => null,
                'subject_type' => $request->type
            ]);

            $expenseId = $entry['expenseId'];

            //reverse the joint expense
            if ($expenseId > 0) {
                $expense = Expense::find($expenseId);
                $expense->update([
                    'reconciled_at' => now(),
                ]);
            }
        });

        return response()->json(['message' => 'Entry added.'], '200');
    }
}
