<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Models\Category;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index()
    {
        return response()->json(['choices' => Category::all()], 200);
    }

    public function store(StoreCategoryRequest $request)
    {
        $this->authorize('create-category');

        Category::create([
            'user_id' => auth('web')->id(),
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return response()->json(['message' => ucfirst($request->name) . ' has been added.'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $model = Category::find($id);

        $model->delete();
    }

    public function restore($id)
    {
        $model = Category::onlyTrashed()->where('id', $id)->first();
        $model->restore();

        return response()->json(['name' => $model->name], 200);
    }
}
