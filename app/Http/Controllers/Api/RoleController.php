<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        $role->delete();
    }


    public function restore($roleSlug)
    {
        $retrieve = Role::onlyTrashed()->where('slug', $roleSlug)->first();
        $retrieve->restore();
    }
}
