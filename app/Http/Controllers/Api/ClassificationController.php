<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Classification;

class ClassificationController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $model = Classification::find($id);

        $model->delete();
    }

    public function restore($id)
    {
        $model = Classification::onlyTrashed()->where('id', $id)->first();
        $model->restore();

        return response()->json(['name' => $model->name], '200');
    }
}
