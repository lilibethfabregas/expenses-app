<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BankTransaction;
use App\Models\CashTransaction;
use App\Models\ChartOfAccounts;
use App\Models\Expense;
use App\Models\Journal;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ReimburseCashJournalController extends Controller
{
    public function store(Request $request)
    {
        //update expenses-> add reconciled_at
        //create journal entry for reversal of expenses and receipt of cash/or payment
        $entries = collect($request->expensesIncluded);

        //get affected expenses in DB and sum the amount, see if balance with the requested amount
        $credits = $entries->reduce(function ($carry, $item) {
            $amount = Expense::find($item)->journal->amount;
            return $carry + $amount;
        });

        if (number_format($credits, 2) !== number_format($request->total, 2)) {
            return response()->json(['message' => 'Not balance'], 422);
        }

        $entries->each(function ($item) {
            $expense = Expense::find($item);
            $expense->update(['reconciled_at' => now()]);
            $expense->journal->reverse();
        });

        $expense = Expense::find($entries->first());

        $journal = Journal::create([
            'user_id' => auth('web')->id(),
            'date' => now(),
            'reference' => 'reversal: ' . $expense->journal->reference,
            'description' => 'reversal',
            'amount' => $request->total,
            'account' => $request->account,
            'is_tracked' => false,
            'subject_id' => $expense->journal->subject_id,
            'subject_type' => $expense->journal->subject_type,
        ]);

        $lastTransaction = CashTransaction::query()->latest()->first();

        //create cash transactions
        $journal->cashTransaction()->create([
            'user_id' => 1,
            'debit' => (int)$request->total >= 1 ? (int)$request->total : 0,
            'credit' => (int)$request->total < 1 ? (int)$request->total : 0,
            'balance' => $lastTransaction ? $lastTransaction->balance + (int)$request->total : (int)$request->total,
            'date' => now(),
        ]);

        return response()->json(['message' => 'Entry added.'], '200');
    }
}
