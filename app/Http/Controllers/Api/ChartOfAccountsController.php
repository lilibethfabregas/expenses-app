<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreChartOfAccountRequest;
use App\Models\ChartOfAccounts;
use App\Models\Classification;

class ChartOfAccountsController extends Controller
{
    public function index()
    {
        $accounts = ChartOfAccounts::all();

        return response()->json(['accounts' => $accounts], '200');
    }

    public function store(StoreChartOfAccountRequest $request)
    {
        $model = ChartOfAccounts::create([
            'user_id' => auth('web')->id(),
            'name' => $request->name,
            'number' => $request->number,
            'classification' => $request->classification,
        ]);

        return response()->json(['account' => $model], '200');
    }
}
