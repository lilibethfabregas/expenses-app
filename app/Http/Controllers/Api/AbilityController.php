<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ability;

class AbilityController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return void
     */
    public function destroy($id)
    {
        $user = Ability::find($id);

        $user->delete();
    }


    public function restore($id)
    {
        $ability = Ability::onlyTrashed()->where('id', $id)->first();
        $ability->restore();

        return response()->json(['name' => $ability->name], '200');
    }
}
