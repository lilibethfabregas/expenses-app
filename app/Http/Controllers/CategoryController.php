<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreCategoryRequest;
use App\Models\Activity;
use App\Models\Category;
use App\Models\Expense;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view-any-category');

        $categories = Category::withTrashed()->latest()->paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\Category')->paginate(config('expenses.paginate_count'));

        return view('categories.index', compact('categories', 'activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-category');

        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(StoreCategoryRequest $request)
    {
        $this->authorize('create-category');

        Category::create([
            'user_id' => auth('web')->id(),
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return response()->json(['message' => ucfirst($request->name) . ' has been added.'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function show(Category $category)
    {
        $this->authorize('view-category');

        return view('categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit(Category $category)
    {
        $this->authorize('update-category');

        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreCategoryRequest $request
     * @param Category $category
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(StoreCategoryRequest $request, Category $category)
    {
        $this->authorize('update-category');

        $relatedExpenses = Expense::query()->where('category', $category->name)->get();

        $category->update([
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        $relatedExpenses->each(function($expense) use ($category) {
            $expense->update(['category' => $category->name]);
        });

        return redirect()->action([self::class, 'show'], $category)
            ->with('flash', 'Category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Category $category)
    {
        $this->authorize('delete-category');

        $category->delete();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $category->name . ' has been deleted.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $categories = Category::query()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->paginate(config('expenses.paginate_count'));

        $count = $categories->count();

        session()->flash(
            'flash',
            $count . ' ' . Str::plural('result', $count) . '  found.'
        );

        $activities = Activity::query()
            ->where('subject_type', 'App\Models\Category')
            ->paginate(config('expenses.paginate_count'));

        return view('categories.index', compact('categories', 'count', 'activities'));
    }
}
