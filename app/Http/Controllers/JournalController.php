<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Activity;
use App\Models\BankTransaction;
use App\Models\ChartOfAccounts;
use App\Models\Journal;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class JournalController extends Controller
{
    public function index()
    {
        $this->authorize('view-any-journal');

        $journals = Journal::paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\Journal')->paginate(config('expenses.paginate_count'));

        return view('journals.index', compact('journals', 'activities'));
    }

    public function create($id)
    {
        $bankTransaction = BankTransaction::find((int) $id);

        $accounts = ChartOfAccounts::all();

        return view('journals.create', compact('bankTransaction', 'accounts'));
    }

    /**
     * 'Show' journal from journals 'Index' page.
     *
     * @param Journal $journal
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function show(Journal $journal)
    {
        $this->authorize('view-journal');

        $transaction = Journal::where('created_at', $journal->created_at)->where('reference', $journal->reference)->get();

        return view('journals.show', compact('transaction', 'journal'));
    }

    /**
     * 'Edit' page from 'Show' journal page.
     *
     * @param Journal $journal
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit(Journal $journal)
    {
        $this->authorize('view-journal');

        $transaction = Journal::where('created_at', $journal->created_at)->where('reference', $journal->reference)->get();

        return view('journals.show', compact('transaction', 'journal'));
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $journals = Journal::query()
            ->where('description', 'LIKE', '%' . $q . '%')
            ->get();

        $archived = Journal::onlyTrashed()
            ->where('description', 'LIKE', '%' . $q . '%')
            ->get();

        $all = $journals->merge($archived);

        session()->flash(
            'flash',
            $all->count() . ' ' . Str::plural('result', $all->count()) . '  found.'
        );

        return view('journals.index', compact('journals', 'archived'));
    }
}
