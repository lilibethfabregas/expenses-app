<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Activity;
use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view-any-user');

        $users = User::withTrashed()->where('role_id', '!=', Role::SUPERADMIN)->paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\User')->paginate(config('expenses.paginate_count'));

        $count = User::withTrashed()->count();

        return view('users.index', compact('users', 'activities', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-user');

        $roles = Role::all();

        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('create-user');

        $user = User::create([
            'name' => ucwords(strtolower($request->name)),
            'slug' => Str::slug($request->name),
            'email' => $request->email,
            'role_id' => $request->role_id,
        ]);

        return redirect()->action([self::class, 'show'], $user)
            ->with('flash', $user->name . ' has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('view-user');

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('update-user');

        $email = substr($user->email, 0, strpos($user->email, '@'));

        $roles = Role::all();

        return view('users.edit', compact('user', 'roles', 'email'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('update-user');
        $user->update([
            'name' => ucwords(strtolower($request->name)),
            'slug' => Str::slug($request->name),
            'email' => strtolower($request->email),
            'role_id' => $request->role_id,
        ]);

        return redirect()->action([self::class, 'show'], $user)
            ->with('flash', $user->name . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(User $user)
    {
        $this->authorize('delete-user');

        $user->delete();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $user->name . ' has been deleted.');
    }

    public function restore($userSlug)
    {
        $retrieve = User::onlyTrashed()->where('slug', $userSlug)->first();
        $retrieve->restore();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $retrieve->name . ' has been restored.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $role = Role::withTrashed()->where('name', 'LIKE', '%' . $q . '%')->pluck('id');

        $users = User::withTrashed()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->orWhere('email', 'LIKE', '%' . $q . '%')
            ->orWhere('id', 'LIKE', '%' . $q . '%')
            ->orWhereIn('role_id', $role)
            ->paginate(config('expenses.paginate_count'));

        $count = $users->count();

        session()->flash(
            'flash',
            $count . ' ' . Str::plural('result', $count) . '  found.'
        );

        $activities = Activity::query()
            ->where('subject_type', 'App\Models\Users')
            ->paginate(config('expenses.paginate_count'));

        return view('users.index', compact('users', 'count', 'activities'));
    }
}
