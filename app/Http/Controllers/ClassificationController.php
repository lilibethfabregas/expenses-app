<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreClassificationRequest;
use App\Models\Activity;
use App\Models\Classification;
use App\Models\Expense;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ClassificationController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view-any-classification');

        $classifications = Classification::withTrashed()->latest()->paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\Classification')->paginate(config('expenses.paginate_count'));

        return view('classifications.index', compact('classifications', 'activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-classification');

        return view('classifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreClassificationRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(StoreClassificationRequest $request)
    {
        $this->authorize('create-classification');

        Classification::create([
            'user_id' => auth('web')->id(),
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return response()->json(['message' => 'Classification added'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Classification $classification
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function show(Classification $classification)
    {
        $this->authorize('view-classification');

        return view('classifications.show', compact('classification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Classification $classification
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit(Classification $classification)
    {
        $this->authorize('update-classification');

        return view('classifications.edit', compact('classification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreClassificationRequest $request
     * @param Classification $classification
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(StoreClassificationRequest $request, Classification $classification)
    {
        $this->authorize('update-classification');

        $relatedExpenses = Expense::query()->where('classification', $classification->name)->get();

        $classification->update([
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        $relatedExpenses->each(function($expense) use ($classification) {
            $expense->update(['classification' => $classification->name]);
        });

        return redirect()->action([self::class, 'show'], $classification)
            ->with('flash', 'Classification has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Classification $classification
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Classification $classification)
    {
        $this->authorize('delete-classification');

        $classification->delete();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $classification->name . ' has been deleted.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $classifications = Classification::query()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->paginate(config('expenses.paginate_count'));

        $count = $classifications->count();

        session()->flash(
            'flash',
            $count . ' ' . Str::plural('result', $count) . '  found.'
        );

        $activities = Activity::query()
            ->where('subject_type', 'App\Models\Classification')
            ->paginate(config('expenses.paginate_count'));

        return view('classifications.index', compact('classifications', 'count', 'activities'));
    }
}
