<?php

namespace App\Http\Controllers;

use App\Models\Broker;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BrokerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        //$this->authorize('create-broker');

        Broker::create([
            //'user_id' => auth('web')->id(),
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return response()->json(['message' => 'Broker added'], 200);
    }
}
