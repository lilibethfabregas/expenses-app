<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStockTransactionRequest;
use App\Models\Broker;
use App\Models\Stock;
use App\Models\StockTransaction;
use Illuminate\Http\Request;

class StockTransactionController extends Controller
{
    public function store(StoreStockTransactionRequest $request)
    {
        $user = auth('web')->user();

        $gross = $request->shares * $request->price;

        $commissionWithVat = (.0025 * $gross) * 1.12;

        $charges = .00015 * $gross; //pseTransFee .00005, sccp .0001

        $stocks = StockTransaction::where('broker_id', $request->broker_id)->get();

        $cashInBroker = $stocks->sum(function ($stock) {
            return $stock->total;
        });

        $type = $request->type;

        $totalBeforeTax = $gross + $commissionWithVat + $charges;
        //make sure you have enough cash in broker
        if ($type === 'BN' && $totalBeforeTax > $cashInBroker) {
            return response()->json(['message' => 'You dont have enough buying power.'], 422);
        }

        $currentShares = StockTransaction::currentShares($request->stock, $request->broker_id);

        //make sure you have enough stock to sell
        if ($type === 'SN' && ($request->shares > (int) $currentShares)) {
            return response()->json(['message' => 'You dont have enough shares to sell.'], 422);
        }

        $salesTx = .006 * $gross;

        $total = $type === 'BN' ? $totalBeforeTax * -1 : $totalBeforeTax - $salesTx;

        $user->stockTransactions()->create([
            'type' => $type,
            'stock' => $request->stock,
            'shares' => $type === 'BN' ? $request->shares : ($request->shares * -1),
            'price' => $request->price,
            'broker_id' => $request->broker_id,
            'charges' => $commissionWithVat + $charges,
            'sales_tx' => $type === 'BN' ? 0 : $salesTx,
            'total' => $total,
            'journal_id' => 1,
            'user_id' => 1
        ]);

        return redirect('/stocks')->with('flash', 'Transaction has been recorded.');
    }

    public function sell(Request $request)
    {
        $user = auth('web')->user();

        $gross = $request->shares * $request->price;

        $commissionWithVat = (.0025 * $gross) * 1.12;

        $charges = .00015 * $gross; //pseTransFee .00005, sccp .0001

        $salesTx = .006 * $gross;

        $user->stockTransactions()->create([
            'type' => 'SN',
            'stock' => $request->stock,
            'shares' => $request->shares,
            'cost' => $request->price,
            'broker_id' => $request->broker_id,
            'charges' => $commissionWithVat + $charges,
            'sales_tx' => $salesTx,
        ]);

        return redirect('/stocks')->with('flash', 'Transaction has been recorded.');
    }

    public function update(Request $request, StockTransaction $stockTransaction)
    {
        $brokerId = Broker::where('name', $request->broker)->first()->id;

        $stockTransaction->update([
            'broker_id' => $brokerId,
        ]);

        return redirect()->action([self::class, 'show'], $stockTransaction)
            ->with('flash', 'Transaction has been updated.');
    }
}
