<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreExpenseRequest;
use App\Http\Requests\UpdateExpenseRequest;
use App\Models\Activity;
use App\Models\Cash;
use App\Models\Expense;
use App\Models\Journal;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class ExpenseController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function index()
    {
        /**@var User $user * */
        $user = auth('web')->user();

        $expenses = $user->expenses()->latest()
            ->paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\Expense')->paginate(config('expenses.paginate_count'));

        $count = Expense::count();

        return view('expenses.index', compact('expenses', 'activities', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        $user = auth('web')->user();

        $categories = $user->categories;

        $classifications = $user->classifications;

        return view('expenses.create', compact('categories', 'classifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreExpenseRequest $request
     * @return RedirectResponse
     */
    public function store(StoreExpenseRequest $request)
    {
        $user = auth('web')->user();

        if ($user->config === null) {
            return back()->with('flash', 'Failed to add. Please setup chart of accounts first.');
        }

        $journal = Journal::create([
            'user_id' => auth('web')->id(),
            'date' => Carbon::parse($request->date)->format('M j, Y'),
            'reference' => 'asdads',
            'description' => $request->particulars,
            'amount' => $request->amount,
            'account' => $request->debit,
            'is_tracked' => true,
        ]);

        $expense = $journal->expense()->create([
            'user_id' => auth('web')->id(),
            'is_joint' => filter_var($request->is_joint, FILTER_VALIDATE_BOOLEAN),
            'category' => $request->category,
        ]);

        $journal->update([
            'reference' => 'Expense Id: ' . $expense->id,
            'subject_id' => $expense->id,
            'subject_type' => Expense::class
        ]);

        //for the credit side
        Journal::create([
            'user_id' => auth('web')->id(),
            'date' => Carbon::parse($request->date)->format('M j, Y'),
            'reference' => 'Expense Id: ' . $expense->id,
            'description' => $request->particulars,
            'amount' => $request->amount,
            'account' => $request->credit,
            'is_tracked' => true,
            'subject_id' => $expense->id,
            'subject_type' => Expense::class
        ]);

        return redirect()->action([self::class, 'show'], $expense)
            ->with('flash', 'New expense has been recorded.');
    }

    /**
     * Display the specified resource.
     *
     * @param Expense $expense
     * @return Application|Factory|View|Response
     */
    public function show(Expense $expense)
    {
        $journal = $expense->journal;

        $credit = Journal::where('reference', $journal->reference)
            ->whereDate('created_at', $journal->created_at)
            ->where('id', '!=', $journal->id)
            ->first()
            ->chartOfAccounts->name;

        return view('expenses.show', compact('expense', 'credit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Expense $expense
     * @return Application|Factory|View|Response
     */
    public function edit(Expense $expense)
    {
        $user = auth('web')->user();

        $categories = $user->categories;

        $classifications = $user->classifications;

        $journal = $expense->journal;
        $credit = Journal::where('reference', $journal->reference)->whereDate('created_at', $journal->created_at)->where('id', '!=', $journal->id)->first()->chartOfAccounts->name;

        return view('expenses.edit', compact('expense', 'categories', 'classifications', 'credit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateExpenseRequest $request
     * @param Expense $expense
     * @return RedirectResponse
     */
    public function update(UpdateExpenseRequest $request, Expense $expense)
    {
        $expense->update([
            'is_joint' => false,
            'category' => $request->category,
        ]);

        $expense->journal()->update([
            'description' => $request->particulars,
            'amount' => $request->amount,
            'account' => $request->debit,
        ]);

        Journal::query()
            ->where('subject_id', $expense->id)
            ->where('subject_type', Expense::class)
            ->where('account', '!=', $expense->journal->account)
            ->first()
            ->update([
                'description' => $request->particulars,
                'amount' => $request->amount,
                'account' => $request->credit,
            ]);

        return redirect()->action([self::class, 'show'], $expense)
            ->with('flash', 'Expense has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Expense $expense
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Expense $expense)
    {
        $this->authorize('delete-expense');

        $expense->delete();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $expense->name . ' has been deleted.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $expenses = Expense::withTrashed()
            ->where('particulars', 'LIKE', '%' . $q . '%')
            ->orWhere('amount', 'LIKE', '%' . $q . '%')
            ->orWhere('category', 'LIKE', '%' . $q . '%')
            ->orWhere('classification', 'LIKE', '%' . $q . '%')
            ->paginate(config('expenses.paginate_count'));

        $count = $expenses->count();

        session()->flash(
            'flash',
            $count . ' ' . Str::plural('result', $count) . '  found.'
        );

        $activities = Activity::query()
            ->where('subject_type', 'App\Models\Expenses')
            ->paginate(config('expenses.paginate_count'));

        return view('expenses.index', compact('expenses', 'count', 'activities'));
    }
}
