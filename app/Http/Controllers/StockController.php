<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Broker;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    public function index()
    {
        $user = auth('web')->user();

        $stocks = $user->stockTransactions()->latest()
            ->paginate(config('expenses.paginate_count'));

        $brokers = $user->brokers;

        $shares = DB::table('stock_transactions as sh')
            ->where('user_id', auth('web')->id())
            ->select(DB::raw('stock as stock'), DB::raw('sum(shares) as total'))
            ->groupBy(DB::raw('stock'))
            ->get();

//        $profit = DB::table('stock_transactions as sh')
//            ->where('user_id', auth('web')->id())
//            ->where('type', 'SN')
//            ->select(DB::raw('stock as stock'), DB::raw('sum(gain) as total'))
//            ->groupBy(DB::raw('stock'))
//            ->get();

        //kunin lahat ng broker, foreach broker, go to stocks, sa stock kunin si journal, then sum the amount in journal
        $buyingPower = $user->brokers->map(function ($broker) {
            return collect($broker)->merge([
                'sum' => $broker->stockTransaction->sum(function ($stock) {
                    return $stock->total;
                }),
            ]);
        });

        $activities = Activity::query()->where('subject_type', 'App\Models\Stock')->limit(10)->latest()->get();

        return view('stocks.index', compact(
            'stocks',
            'activities',
            'brokers',
            'shares',
            'buyingPower',
        ));
    }

    public function update($stockId, Request $request)
    {
        $stock = Stock::find((int)$stockId);

        $broker = Broker::where('name', $request->broker)->first();

        if (!$broker) {
            return response()->json(['message' => 'Broker does not exist. Add first.'], 422);
        }

        $stock->update(['broker_id' => $broker->id]);

        return response()->json(['message' => 'Stock with ID: ' . ucfirst($stock->id) . ' has been updated.'], 200);
    }
}
