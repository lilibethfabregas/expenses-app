<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreCashRequest;
use App\Http\Requests\UpdateCashRequest;
use App\Models\Cash;
use App\Models\Activity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CashController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $cash = Cash::withTrashed()->get();

        $activities = Activity::query()->where('subject_type', 'App\Models\Cash')->limit(10)->latest()->get();

        return view('cash-transaction.index', compact('cash', 'activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('cash-transaction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCashRequest $request
     * @return RedirectResponse
     */
    public function store(StoreCashRequest $request)
    {
        $cash = Cash::create([
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $cash)
            ->with('flash', $cash->name . ' has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param Cash $cash
     * @return Application|Factory|View|Response
     */
    public function show(Cash $cash)
    {
        return view('cash-transaction.show', compact('cash'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Cash $cash
     * @return Application|Factory|View|Response
     */
    public function edit(Cash $cash)
    {
        return view('cash-transaction.edit', compact('cash'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCashRequest $request
     * @param Cash $cash
     * @return RedirectResponse
     */
    public function update(UpdateCashRequest $request, Cash $cash)
    {
        $cash->update([
            'name' => ucfirst(strtolower(str_replace('-', ' ', $request->name))),
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $cash)
            ->with('flash', $cash->name . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cash $cash
     * @return RedirectResponse
     */
    public function destroy(Cash $cash)
    {
        return redirect()->action([self::class, 'index'])
            ->with('flash', $cash->name . ' deleted.');
    }

    public function restore($cashSlug)
    {
        $retrieve = Cash::onlyTrashed()->where('slug', $cashSlug)->first();
        $retrieve->restore();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $retrieve->name . ' has been restored.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $cash = Cash::query()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->get();

        $archived = Cash::onlyTrashed()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->get();

        $all = $cash->merge($archived);

        session()->flash(
            'flash',
            $all->count() . ' ' . Str::plural('result', $all->count()) . '  found.'
        );

        return view('cash-transaction.index', compact('cash', 'archived'));
    }
}
