<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $user = auth('web')->user();

        $ytdIncome = $user->journals()->ytdIncome();

        $ytdExpenses = $user->journals()->ytdExpenses();

        $cash = $user->journals()->ytdCash();

        $stocks = $user->journals()->ytdStocks();

        $totalUnreconExpenses = $user->expenses()
            ->unreconciled()
            ->get()
            ->sum(function ($expense) {
                return $expense->journal->amount;
            });

        $unreconExpenses = $user->expenses()
            ->unreconciled()
            ->get();

        $totalUnreconBankTrans = $user->bankTransactions()
                ->whereNull('recorded_at')
                ->sum('debit') -
            $user->bankTransactions()
                ->whereNull('recorded_at')
                ->sum('credit');

        $unreconBankTrans = $user->bankTransactions()
                ->whereNull('recorded_at')->take(2)->get();

        return view('dashboard',
            compact(
                'ytdIncome',
                'ytdExpenses',
                'cash',
                'stocks',
                'totalUnreconExpenses',
                'unreconExpenses',
                'totalUnreconBankTrans',
                'unreconBankTrans')
        );
    }
}
