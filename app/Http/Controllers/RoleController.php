<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Models\Ability;
use App\Models\Activity;
use App\Models\Role;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view-any-role');

        $roles = Role::withTrashed()->paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\Role')->paginate(config('expenses.paginate_count'));

        $count = Role::withTrashed()->count();

        return view('roles.index', compact('roles', 'activities', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-role');

        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRoleRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(StoreRoleRequest $request)
    {
        $this->authorize('create-role');

        $role = Role::create([
            'name' => ucwords(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return response()->json(['message' => ucfirst($role->name) . ' has been added.'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Role $role
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function show(Role $role)
    {
        $this->authorize('view-role');

        $ability_role = $role->abilities->pluck('name')->unique();

        $abilities = Ability::query()->whereNotIn('name', $ability_role)->get();

        return view('roles.show', compact('role', 'abilities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit(Role $role)
    {
        $this->authorize('update-role');

        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRoleRequest $request
     * @param Role $role
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $this->authorize('update-role');

        $role->update([
            'name' => ucwords(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        if ($request->ability) {
            $role->allowTo($request->ability);
        }

        return redirect()->action([self::class, 'show'], $role)
            ->with('flash', $role->name . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Role $role)
    {
        $this->authorize('delete-role');

        $role->delete();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $role->name . ' has been deleted.');
    }

    public function restore($roleSlug)
    {
        $retrieved = Role::onlyTrashed()->where('slug', $roleSlug)->first();
        $retrieved->restore();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $retrieved->name . ' has been restored.');
    }
}
