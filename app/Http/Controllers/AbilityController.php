<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Http\Requests\StoreAbilityRequest;
use App\Http\Requests\UpdateAbilityRequest;
use App\Models\Ability;
use App\Models\Activity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class AbilityController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view-any-ability');
        $abilities = Ability::withTrashed()->paginate(config('expenses.paginate_count'));

        $activities = Activity::query()->where('subject_type', 'App\Models\Ability')->paginate(config('expenses.paginate_count'));

        $count = Ability::withTrashed()->count();

        return view('abilities.index', compact('abilities', 'activities', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create-ability');

        return view('abilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAbilityRequest $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function store(StoreAbilityRequest $request)
    {
        $this->authorize('create-ability');

        $ability = Ability::create([
            'name' => ucfirst(strtolower($request->name)),
            'slug' => Str::slug($request->name),
        ]);

        return response()->json(['message' => ucfirst($ability->name) . ' has been added.'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Ability $ability
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function show(Ability $ability)
    {
        $this->authorize('view-ability');

        return view('abilities.show', compact('ability'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Ability $ability
     * @return Application|Factory|View|Response
     * @throws AuthorizationException
     */
    public function edit(Ability $ability)
    {
        $this->authorize('update-ability');

        return view('abilities.edit', compact('ability'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAbilityRequest $request
     * @param Ability $ability
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateAbilityRequest $request, Ability $ability)
    {
        $this->authorize('update-ability');

        $ability->update([
            'name' => ucfirst(strtolower(str_replace('-', ' ', $request->name))),
            'slug' => Str::slug($request->name),
        ]);

        return redirect()->action([self::class, 'show'], $ability)
            ->with('flash', $ability->name . ' has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ability $ability
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Ability $ability)
    {
        $this->authorize('delete-ability');

        if ($ability->roles->isEmpty()) {
            $ability->delete();

            return redirect()->action([self::class, 'index'])
                ->with('flash', $ability->name . ' has been deleted.');
        }

        return redirect()->action([self::class, 'index'])
            ->with('flash', $ability->name . ' cannot be deleted, a role is using it.');
    }

    public function restore($abilitySlug)
    {
        $retrieve = Ability::onlyTrashed()->where('slug', $abilitySlug)->first();
        $retrieve->restore();

        return redirect()->action([self::class, 'index'])
            ->with('flash', $retrieve->name . ' has been restored.');
    }

    public function search(SearchRequest $request)
    {
        $q = $request->input('q');

        $abilities = Ability::withTrashed()
            ->where('name', 'LIKE', '%' . $q . '%')
            ->paginate(config('expenses.paginate_count'));

        $count = $abilities->count();

        session()->flash('flash',$count . ' ' . Str::plural('result', $count) . '  found.');

        $activities = Activity::query()
            ->where('subject_type', 'App\Models\Ability')
            ->paginate(config('expenses.paginate_count'));

        return view('abilities.index', compact('abilities','activities', 'count'));
    }
}
