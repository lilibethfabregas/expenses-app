<?php

namespace App\Http\Controllers;

use EllipseSynergie\ApiResponse\Laravel\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Manager;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var Response
     */
    protected $response;

    public function __construct()
    {
        $fractal = new Manager();
        if (request()->query->has('include')) {
            $fractal->parseIncludes(request()->query('include'));
        }
        $this->response = new Response($fractal);
    }
}
