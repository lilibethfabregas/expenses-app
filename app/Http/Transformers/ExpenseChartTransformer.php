<?php

namespace App\Http\Transformers;

use App\Models\Expense;
use League\Fractal\TransformerAbstract;

class ExpenseChartTransformer extends TransformerAbstract
{
    public function transform(Expense $expense)
    {
        return [
            'date' => $expense->created_at->format('M'),
            'amount' => $expense->journal->amount,
        ];
    }
}
