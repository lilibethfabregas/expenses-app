<?php

namespace App\Http\Transformers;

use App\Models\Expense;
use League\Fractal\TransformerAbstract;

class ExpenseTransformer extends TransformerAbstract
{
    public function transform(Expense $expense)
    {
        return [
            "date" => $expense->journal->date->format('M d, Y'),
            "accountNumber" => $expense->journal->chartOfAccounts->number,
            'accountName' => $expense->journal->chartOfAccounts->name,
            'expenseId' => $expense->id,
            "description" => $expense->journal->description,
            "amount" => $expense->journal->amount,
            "isTracked" => false,
            "isSelected" => false,
        ];
    }
}
