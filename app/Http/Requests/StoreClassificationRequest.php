<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClassificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:classifications,name',
        ];
    }

    /**
     * Get the display messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.unique' => 'This classification is already existing.',
        ];
    }
}
