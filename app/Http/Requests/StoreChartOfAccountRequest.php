<?php

namespace App\Http\Requests;

use App\Rules\CoaClassificationMustBeValid;
use Illuminate\Foundation\Http\FormRequest;

class StoreChartOfAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'name' => 'required',
            'classification' => ['required', new CoaClassificationMustBeValid()],
        ];
    }
}
