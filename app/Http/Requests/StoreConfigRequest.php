<?php

namespace App\Http\Requests;

use App\Rules\AccountMustBeAnAsset;
use App\Rules\AccountMustHaveValidDigits;
use App\Rules\AccountMustNotOverlap;
use Illuminate\Foundation\Http\FormRequest;

class StoreConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'digit' => ['required'],
            'assetStart' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'assetEnd' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'liabilityStart' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'liabilityEnd' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'equityStart' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'equityEnd' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'incomeStart' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'incomeEnd' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'expensesStart' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'expensesEnd' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustNotOverlap($this->request),
            ],
            'cashOnHand' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustBeAnAsset($this->request),
            ],
            'cashInBroker' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustBeAnAsset($this->request),
            ],
            'inventory' => [
                'required',
                'integer',
                new AccountMustHaveValidDigits($this->request),
                new AccountMustBeAnAsset($this->request),
            ],
        ];
    }
}
