<?php

use App\Http\Controllers\AbilityController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\BankTransactionController;
use App\Http\Controllers\BrokerController;
use App\Http\Controllers\CashController;
use App\Http\Controllers\CashTransactionController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ClassificationController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\StockTransactionController;
use App\Http\Controllers\ThirdPartyController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::view('/authenticate', 'auth.authenticate');
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::view('/', 'home');

Route::resource('/abilities', AbilityController::class);
    Route::post('/abilities/search', [AbilityController::class, 'search'])->name('abilities.search');
    Route::get('/abilities/{abilitySlug}/restore', [AbilityController::class, 'restore'])->name('abilities.restore');

Route::resource('/roles', RoleController::class);
Route::get('/abilities/{roleSlug}/restore', [RoleController::class, 'restore'])->name('abilities.restore');

Route::resource('users', UserController::class);
    Route::post('/users/search', [UserController::class, 'search'])->name('users.search');
    Route::get('/users/{userSlug}/restore', [UserController::class, 'restore'])->name('users.restore');

Route::resource('/categories', CategoryController::class);
Route::resource('/classifications', ClassificationController::class);

Route::resource('/expenses', ExpenseController::class);
    Route::post('/expenses/search', [ExpenseController::class, 'search'])->name('expenses.search');

//Route::resource('/cash', CashController::class);
Route::get('/cash-transactions/receive', [CashTransactionController::class, 'receive']);
Route::get('/cash-transactions/create', [CashTransactionController::class, 'create']);
Route::resource('/cash-transactions', CashTransactionController::class)->only(['index', 'show']);
Route::resource('/banks', BankController::class);

Route::resource('banks', BankController::class);
    Route::post('/banks/search', [BankController::class, 'search'])->name('banks.search');
    Route::get('/banks/{bankSlug}/restore', [BankController::class, 'restore'])->name('banks.restore');

    Route::post('bank-transactions', BankTransactionController::class);
    Route::get('bank-transactions/{id}/create-journal', [JournalController::class, 'create'])
        ->name('bank-transactions.journals.create');

Route::resource('journals', JournalController::class);

Route::view('stock-transaction/buy', 'stocks.buy')->name('stock-transaction.buy.create');
//Route::post('stock-transaction/buy', [StockTransactionController::class, 'buy'])->name('stock-transaction.buy.store');
Route::view('stock-transaction/sell', 'stocks.sell')->name('stock-transaction.sell.create');
//Route::post('stock-transaction/sell', [StockTransactionController::class, 'sell'])->name('stock-transaction.sell.store');

Route::post('vendors', [ThirdPartyController::class, 'store']);

Route::resource('stock-transaction', StockTransactionController::class);
Route::resource('stocks', StockController::class)->only(['index', 'create', 'show', 'update']);
Route::resource('brokers', BrokerController::class)->only(['store']);

Route::view('profile', 'profile.show');

