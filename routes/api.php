<?php

use App\Http\Controllers\Api\AbilityController;
use App\Http\Controllers\Api\BrokerController;
use App\Http\Controllers\Api\ReceiveCashJournalController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ChartOfAccountsController;
use App\Http\Controllers\Api\ClassificationController;
use App\Http\Controllers\Api\ConfigController;
use App\Http\Controllers\Api\ExpenseController;
use App\Http\Controllers\Api\JournalController;
use App\Http\Controllers\Api\ReimburseCashJournalController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\ThirdPartyController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('/users/{slug}/restore', [UserController::class, 'restore']);
Route::delete('/users/{id}', [UserController::class, 'destroy']);

Route::get('/roles/{slug}/restore', [RoleController::class, 'restore']);
Route::delete('/roles/{id}', [RoleController::class, 'destroy']);

Route::get('/abilities/{slug}/restore', [AbilityController::class, 'restore']);
Route::delete('/abilities/{id}', [AbilityController::class, 'destroy']);

Route::get('/third-parties', [ThirdPartyController::class, 'index']);
Route::post('/third-parties', [ThirdPartyController::class, 'store']);

Route::get('/categories', [CategoryController::class, 'index']);
Route::post('/categories', [CategoryController::class, 'store']);
Route::get('/categories/{slug}/restore', [CategoryController::class, 'restore']);
Route::delete('/categories/{id}', [CategoryController::class, 'destroy']);

Route::put('/expenses/{expenseId}', [ExpenseController::class, 'update']);

Route::get('/classifications/{slug}/restore', [ClassificationController::class, 'restore']);
Route::delete('/classifications/{id}', [ClassificationController::class, 'destroy']);

Route::post('/journal', [JournalController::class, 'store']);
Route::post('/journal/receive-cash', [ReceiveCashJournalController::class, 'storeCashTransaction']);

Route::post('/reimburse', [ReimburseCashJournalController::class, 'store']);

Route::post('/chart-of-accounts', [ChartOfAccountsController::class, 'store']);
Route::get('/chart-of-accounts', [ChartOfAccountsController::class, 'index']);

Route::post('/broker', [BrokerController::class, 'store']);

Route::get('/unreconciled-reimbursable-expenses', [ExpenseController::class, 'getUnreconciledReimbursable']);

Route::get('charts/expenses', [ExpenseController::class, 'chart']);

Route::post('config', [ConfigController::class, 'store']);

