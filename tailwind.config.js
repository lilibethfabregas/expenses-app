module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            zIndex: {
                "-1": "-1",
            },
          colors: {
            navbar: 'rgba(23,116,147,0.31)',
            primaryLightest: '#5a7177',
            primaryLighter: '#21afd6',
            primary: '#0881a3',
            primaryDarker: '#013640',
            background: '#f5f5f5',
          },
          fontFamily: {
            'sans': ['Montserrat'],
            'serif': ['New Tegomin'],
            'cursive': ['Potta One'],
            'display': ['Arima Madurai'],
          }
        },
    },
    variants: {
      extend: {
        divideColor: ['group-hover'],
      },
        borderColor: ['responsive', 'hover', 'focus', 'focus-within'],
        zIndex: ['hover', 'active'],

    },
    plugins: [],
}
